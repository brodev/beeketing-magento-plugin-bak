<?php
/**
 * Adminhtml index
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace Beeketing\MagentoConnect\Block\Adminhtml\Index;

use BeeketingConnect\Platforms\Magento\Helper\Snippet;
use Magento\Framework\View\Element\Template;

class Index extends Template
{
    /**
     * @var Snippet
     */
    private $snippetHelper;

    /**
     * Index constructor.
     * @param Template\Context $context
     * @param Snippet $snippet
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Snippet $snippet,
        array $data = []
    ) {

        parent::__construct($context, $data);
        $this->snippetHelper = $snippet;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getJSAppData()
    {
        $data = $this->snippetHelper->getJsAppData();
        $data['plugin_url'] = $this->getViewFileUrl('Beeketing_MagentoConnect');
        return json_encode($data);
    }

    /**
     * @return string
     */
    public function getDashBoardUrl()
    {
        return $this->snippetHelper->getConnectDashboardUrl('app.js', true);
    }

    /**
     * @return string
     */
    public function getAnalyticsUrl()
    {
        return $this->snippetHelper->getAnalyticsUrl(true);
    }
}
