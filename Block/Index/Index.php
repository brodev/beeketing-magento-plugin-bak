<?php
/**
 * Frontend index
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace Beeketing\MagentoConnect\Block\Index;

use BeeketingConnect\Platforms\Magento\Helper\Snippet;
use Magento\Framework\View\Element\Template;

class Index extends Template
{
    /**
     * @var Snippet
     */
    private $snippetHelper;

    /**
     * Index constructor.
     * @param Template\Context $context
     * @param Snippet $snippet
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Snippet $snippet,
        array $data = []
    ) {

        parent::__construct($context, $data);
        $this->snippetHelper = $snippet;
    }

    /**
     * Get js app data
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSnippet()
    {
        $this->snippetHelper->init();
        if (!$this->snippetHelper->getApiKey()) {
            return '';
        }
        return $this->snippetHelper->getPageSnippetData()
            . $this->snippetHelper->getBeeketingScript();
    }
}
