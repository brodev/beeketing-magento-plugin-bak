Magento 2 Beeketing Extension
==========================

This library includes the files of the Beeketing extension.
The directories hierarchy is as positioned in a standard magento 2 project library

This library will also include different version packages as magento 2 extensions

## Requirements

magento 2.0 +

## Installation

The Magento 2 module can be installed with Composer (https://getcomposer.org/download/).
From the command line, do the following in your Magento 2 installation directory:

```
composer require beeketing/magento-beeketing
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento cache:flush
```

### Usage

After the installation, Go to the Magento 2 admin panel and select Beeketing