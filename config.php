<?php
define('BEEKETINGMAGENTO_PATH', 'https://go.beeketing.com');
define('BEEKETINGMAGENTO_DASHBOARD', 'https://fly.beeketing.com');
define('BEEKETINGMAGENTO_API', 'https://api.beeketing.com');
define('BEEKETINGMAGENTO_GO_API', 'https://gapi.beeketing.com');
define('BEEKETINGMAGENTO_ENVIRONMENT', 'prod');
define('BEEKETINGMAGENTO_CONNECT_DASHBOARD', 'https://connect-dashboard.beeketing.com');
define('BEEKETINGMAGENTO_VERSION', '1.1.6');
define('BEEKETINGMAGENTO_SDK_PATH', 'https://sdk.beeketing.com/js/beeketing.js');
define('BEEKETINGMAGENTO_ANALYTICS', 'https://sdk.beeketing.com/analytics/analytics.min.js');
