<?php
/**
 * Admin ajax controller
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace Beeketing\MagentoConnect\Controller\Adminhtml\Index;

use BeeketingConnect\Platforms\Magento\Core\Api\App;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\View\Result\PageFactory;

class Ajax extends Action
{

    /**
     * Result page factory
     *
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * Json helper
     *
     * @var Data
     */
    private $jsonHelper;

    /**
     * Module app api
     *
     * @var App
     */
    private $app;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Data $jsonHelper
     * @param App $app
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Data $jsonHelper,
        App $app
    ) {

        parent::__construct($context);

        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->app = $app;
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $this->app->init();
        if ($this->getRequest()->isAjax()) {
            try {
                $action = $this->getRequest()->getPost('action');
                switch ($action) {
                    case 'verify_account':
                        return $this->registerShop();
                }
            } catch (LocalizedException $e) {
                return $this->jsonResponse($e->getMessage());
            } catch (\Exception $e) {
                return $this->jsonResponse($e->getMessage());
            }
        }
    }

    /**
     * Register Shop
     * @return ResultInterface
     * @throws NoSuchEntityException
     */
    private function registerShop()
    {
        $this->app->init();
        $apiKey = $this->getRequest()->getPost('api_key');
        $userId = $this->getRequest()->getPost('user_id');
        $autoLoginToken = $this->getRequest()->getPost('auto_login_token');
        if ($apiKey && $userId && $autoLoginToken) {
            $this->app->setApiKey($apiKey);
            $apiKey = $this->app->registerShop($userId, $autoLoginToken);

            return $this->jsonResponse([
                'success' => true,
                'data' => [
                    'api_key' => $apiKey,
                ]
            ]);
        }

        return $this->jsonResponse([
            'success' => false,
        ]);
    }

    /**
     * Create json response
     *
     * @param string $response
     * @return ResultInterface
     */
    private function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
