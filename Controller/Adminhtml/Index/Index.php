<?php
/**
 * Admin index controller
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace Beeketing\MagentoConnect\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * Result page controller
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     * Module app api
     *
     * @var \BeeketingConnect\Platforms\Magento\Core\Api\App
     */
    private $app;

    /**
     * Index constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \BeeketingConnect\Platforms\Magento\Core\Api\App $app
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \BeeketingConnect\Platforms\Magento\Core\Api\App $app
    ) {

        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->app = $app;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $this->app->init();
        $this->app->detectDomainChange();
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(static::ADMIN_RESOURCE);

        return $resultPage;
    }
}
