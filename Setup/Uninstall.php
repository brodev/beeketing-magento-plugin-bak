<?php
/**
 * Module uninstall
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace Beeketing\MagentoConnect\Setup;

use BeeketingConnect\Platforms\Magento\Constants;
use BeeketingConnect\Platforms\Magento\Core\Api\App;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;

class Uninstall implements UninstallInterface
{
    /**
     * Module app api
     *
     * @var App
     */
    private $app;

    /**
     * @var ScopeInterface
     */
    private $scopeConfig;

    /**
     * @var ConfigInterface
     */
    private $resourceConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Uninstall constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param ConfigInterface $resourceConfig
     * @param StoreManagerInterface $storeManager
     * @param App $app
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ConfigInterface $resourceConfig,
        StoreManagerInterface $storeManager,
        App $app
    ) {
        $this->app = $app;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        // Send webhook and remove settings
        $this->app->init();
        $settingHelper = $this->app->getSettingHelper();

        // Send tracking
        $email = $this->scopeConfig->getValue(
            'trans_email/ident_general/email',
            ScopeInterface::SCOPE_STORE
        );

        $this->app->sendTrackingEvent([
            'event' => Constants::PLUGIN_EVENT_UNINSTALL,
        ], $email);

        // Delete plugin setting
        $settingHelper->setAppSettingKey(Constants::BEEKETING_CONNECT_KEY);
        $this->app->setAppCode(null);

        $this->resourceConfig->deleteConfig(
            Constants::BEEKETING_CONNECT_KEY,
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            Store::DEFAULT_STORE_ID
        );

        $settingHelper->deleteAllSettings();
        $setup->endSetup();
    }
}
