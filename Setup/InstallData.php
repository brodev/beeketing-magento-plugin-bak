<?php
/**
 * Module install
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace Beeketing\MagentoConnect\Setup;

use BeeketingConnect\Common\Constants as CommonConstants;
use BeeketingConnect\Platforms\Magento\Constants;
use BeeketingConnect\Platforms\Magento\Core\Api\App;
use BeeketingConnect\Platforms\Magento\Helper\Common;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\ScopeInterface;

class InstallData implements InstallDataInterface
{
    /**
     * Module app api
     *
     * @var App
     */
    private $app;

    /**
     * @var ScopeInterface
     */
    private $scopeConfig;

    /**
     * @var Common
     */
    private $commonHelper;

    /**
     * InstallData constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param App $app
     * @param Common $commonHelper
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        App $app,
        Common $commonHelper
    ) {
        $this->app = $app;
        $this->scopeConfig = $scopeConfig;
        $this->commonHelper = $commonHelper;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->app->init();

        $settingHepler = $this->app->getSettingHelper();
        $settingHepler->updateSettings(CommonConstants::SETTING_SITE_URL, $this->commonHelper->getShopAbsolutePath());

        // Send tracking
        $email = $this->scopeConfig->getValue(
            'trans_email/ident_general/email',
            ScopeInterface::SCOPE_STORE
        );

        $this->app->sendTrackingEvent([
            'event' => Constants::PLUGIN_EVENT_INSTALL,
        ], $email);
    }
}
