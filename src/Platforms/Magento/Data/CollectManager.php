<?php

namespace BeeketingConnect\Platforms\Magento\Data;

use BeeketingConnect\Common\Constants as CommonConstants;
use BeeketingConnect\Common\Data\Factory;
use BeeketingConnect\Common\Data\Manager\CollectManagerAbstract;
use BeeketingConnect\Common\Data\Model\Collect;
use BeeketingConnect\Common\Data\Model\Collection;
use BeeketingConnect\Common\Data\Model\Count;
use BeeketingConnect\Common\Data\Model\ResourceParams;
use BeeketingConnect\Platforms\Magento\Helper\Setting;

class CollectManager extends CollectManagerAbstract
{
    /**
     * @var mixed
     */
    private $resourceConnection;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    private $visibility;

    /**
     * @var Setting
     */
    private $settingHelper;

    /**
     * @var Factory
     */
    private $dataModelFactory;

    /**
     * CollectManager constructor.
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Catalog\Model\Product\Visibility $visibility
     * @param Setting $settingHelper
     * @param Factory $dataModelFactory
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\Product\Visibility $visibility,
        Setting $settingHelper,
        Factory $dataModelFactory
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->visibility = $visibility;
        $this->settingHelper = $settingHelper;
        $this->dataModelFactory = $dataModelFactory;
    }

    /**
     * @return mixed
     */
    private function getConnection()
    {
        return $this->resourceConnection->getConnection();
    }

    /**
     * @param $name
     * @return mixed
     */
    private function getTableName($name)
    {
        return $this->resourceConnection->getTableName($name);
    }

    /**
     * Count collects
     * @return int
     */
    public function getCollectsCount()
    {
        $storeId = $this->settingHelper->getCurrentStoreId();
        // @codingStandardsIgnoreStart
        $select = $this->getConnection()->select()
            ->from($this->getTableName('catalog_category_product_index'), 'COUNT(*)')
            ->where('store_id = ?', $storeId)
            ->where('visibility IN (?)', $this->visibility->getVisibleInSiteIds());
        $result = $this->getConnection()->fetchOne($select);
        // @codingStandardsIgnoreEnd

        return (int)$result;
    }

    /**
     * Count collects by collection id
     * @param $collectionId
     * @return int
     */
    public function getCollectsCountByCollectionId($collectionId)
    {
        $storeId = $this->settingHelper->getCurrentStoreId();
        // @codingStandardsIgnoreStart
        $select = $this->getConnection()->select()
            ->from($this->getTableName('catalog_category_product_index'), 'COUNT(*)')
            ->where('category_id=?', $collectionId)
            ->where('store_id = ?', $storeId)
            ->where('visibility IN (?)', $this->visibility->getVisibleInSiteIds());
        $result = $this->getConnection()->fetchOne($select);
        // @codingStandardsIgnoreEnd

        return (int)$result;
    }

    /**
     * Count collects by product id
     * @param $productId
     * @return int
     */
    public function getCollectsCountByProductId($productId)
    {
        $storeId = $this->settingHelper->getCurrentStoreId();
        // @codingStandardsIgnoreStart
        $select = $this->getConnection()->select()
            ->from($this->getTableName('catalog_category_product_index'), 'COUNT(*)')
            ->where('product_id=?', $productId)
            ->where('store_id = ?', $storeId)
            ->where('visibility IN (?)', $this->visibility->getVisibleInSiteIds());
        $result = $this->getConnection()->fetchOne($select);
        // @codingStandardsIgnoreEnd

        return (int)$result;
    }

    /**
     * Get collects
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getCollects(
        $page = CommonConstants::DEFAULT_PAGE,
        $limit = CommonConstants::DEFAULT_ITEMS_PER_PAGE
    ) {
        $storeId = $this->settingHelper->getCurrentStoreId();
        // @codingStandardsIgnoreStart
        $select = $this->getConnection()->select()
            ->from($this->getTableName('catalog_category_product_index'))
            ->where('store_id = ?', $storeId)
            ->where('visibility IN (?)', $this->visibility->getVisibleInSiteIds())
            ->limitPage($page, $limit);
        $result = $this->getConnection()->fetchAll($select);
        // @codingStandardsIgnoreEnd

        $results = [];
        if ($result) {
            foreach ($result as $item) {
                $results[] = $this->formatCollect($item);
            }
        }

        return $results;
    }

    /**
     * Get collects by collection id
     * @param $collectionId
     * @param $page
     * @param $limit
     * @return array
     */
    public function getCollectsByCollectionId(
        $collectionId,
        $page = CommonConstants::DEFAULT_PAGE,
        $limit = CommonConstants::DEFAULT_ITEMS_PER_PAGE
    ) {
        $storeId = $this->settingHelper->getCurrentStoreId();
        // @codingStandardsIgnoreStart
        $select = $this->getConnection()->select()
            ->from($this->getTableName('catalog_category_product_index'))
            ->where('category_id=?', $collectionId)
            ->where('store_id = ?', $storeId)
            ->where('visibility IN (?)', $this->visibility->getVisibleInSiteIds())
            ->limitPage($page, $limit);
        $result = $this->getConnection()->fetchAll($select);
        // @codingStandardsIgnoreEnd

        $results = [];
        if ($result) {
            foreach ($result as $item) {
                $results[] = $this->formatCollect($item);
            }
        }

        return $results;
    }

    /**
     * Get collects by product id
     * @param $productId
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getCollectsByProductId(
        $productId,
        $page = CommonConstants::DEFAULT_PAGE,
        $limit = CommonConstants::DEFAULT_ITEMS_PER_PAGE
    ) {
        $storeId = $this->settingHelper->getCurrentStoreId();
        // @codingStandardsIgnoreStart
        $select = $this->getConnection()->select()
            ->from($this->getTableName('catalog_category_product_index'))
            ->where('product_id=?', $productId)
            ->where('store_id = ?', $storeId)
            ->where('visibility IN (?)', $this->visibility->getVisibleInSiteIds())
            ->limitPage($page, $limit);
        $result = $this->getConnection()->fetchAll($select);
        // @codingStandardsIgnoreEnd

        $results = [];
        if ($result) {
            foreach ($result as $item) {
                $results[] = $this->formatCollect($item);
            }
        }

        return $results;
    }

    /**
     * Format collect
     * @param $collect
     * @return Collect
     */
    private function formatCollect($collect)
    {
        $coll = $this->dataModelFactory->create(Collect::class, [
            'id' => $collect['category_id'] * 100000 + $collect['product_id'] + $collect['store_id'],
            'collection_id' => (int)$collect['category_id'],
            'product_id' => (int)$collect['product_id'],
            'position' => (int)$collect['position'],
        ]);

        return $coll;
    }

    /**
     * Get many collects
     *
     * @param $arg
     * @return array|Collect[]
     */
    public function getMany($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);

        if ($params->getCollectionId()) {
            $collects = $this->getCollectsByCollectionId(
                $params->getCollectionId(),
                $params->getPage(),
                $params->getLimit()
            );
        } elseif ($params->getProductId()) {
            $collects = $this->getCollectsByProductId(
                $params->getProductId(),
                $params->getPage(),
                $params->getLimit()
            );
        } else {
            $collects = $this->getCollects($params->getPage(), $params->getLimit());
        }

        return $collects;
    }

    /**
     * @param $arg
     * @return Count
     */
    public function count($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);

        if ($params->getCollectionId()) {
            $count = $this->getCollectsCountByCollectionId($params->getCollectionId());
        } elseif ($params->getProductId()) {
            $count = $this->getCollectsCountByProductId($params->getProductId());
        } else {
            $count = $this->getCollectsCount();
        }

        return $this->dataModelFactory->create(
            Count::class,
            ['count' => $count]
        );
    }
}
