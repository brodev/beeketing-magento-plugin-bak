<?php

namespace BeeketingConnect\Platforms\Magento\Data;

use BeeketingConnect\Common\Data\CommonHelper;
use BeeketingConnect\Common\Data\Factory;
use BeeketingConnect\Common\Data\Manager\CollectionManagerAbstract;
use BeeketingConnect\Common\Data\Model\Collection;
use BeeketingConnect\Common\Data\Model\Count;
use BeeketingConnect\Common\Data\Model\ResourceParams;
use BeeketingConnect\Platforms\Magento\Helper\Setting;

class CollectionManager extends CollectionManagerAbstract
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator
     */
    private $categoryUrlPathGenerator;

    /**
     * @var Setting
     */
    private $settingHelper;

    /**
     * @var Factory
     */
    private $dataModelFactory;

    /**
     * CollectionManager constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory
     * @param \Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator $categoryUrlPathGenerator
     * @param Setting $settingHelper
     * @param Factory $dataModelFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory,
        \Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator $categoryUrlPathGenerator,
        Setting $settingHelper,
        Factory $dataModelFactory
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->categoryUrlPathGenerator = $categoryUrlPathGenerator;
        $this->settingHelper = $settingHelper;
        $this->dataModelFactory = $dataModelFactory;
    }

    /**
     * @param \Magento\Catalog\Model\Category $collection
     * @return Collection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function formatCollection(\Magento\Catalog\Model\Category $collection)
    {
        $beeCollection = $this->dataModelFactory->create(Collection::class, [
            'id' => (int)$collection->getId(),
            'title' => $collection->getName(),
            'handle' => $this->categoryUrlPathGenerator->getUrlPathWithSuffix($collection, null),
            'published_at' => CommonHelper::formatDate($collection->getCreatedAt()),
            'image_url' => $collection->getImageUrl(),
        ]);

        return $beeCollection;
    }

    /**
     * Get one collection
     * @param $arg
     * @return Collection|null
     */
    public function get($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);
        $result = $this->collectionFactory->create();
        $result->addAttributeToSelect('*')
            ->addIdFilter($params->getResourceId())
            ->addFieldToFilter('path', ['neq' => '1/2'])
            ->addIsActiveFilter()
            ->setPageSize(1)
            ->setCurPage(1);

        if ($result->getSize()) {
            return $this->formatCollection($result->getFirstItem());
        }

        return [];
    }

    /**
     * Get many collection
     * @param $arg
     * @return array
     */
    public function getMany($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);
        $limit = $params->getLimit();
        $page = $params->getPage();
        $title = $params->getTitle();

        $args = [
            'hide_empty' => true,
        ];

        // Limit
        if ($limit) {
            $args['number'] = $limit;
        }

        // Offset
        if ($page) {
            $page--;
            $args['offset'] = $page * $limit;
        }

        // Title
        if ($title) {
            $args['search'] = $title;
        }

        $storeId = $this->settingHelper->getCurrentStoreId();
        $result = $this->collectionFactory->create();
        $result->addAttributeToSelect('*');
        $result->addFieldToFilter('path', ['neq' => '1/2']);
        $result->addIsActiveFilter();
        $result->setStoreId($storeId);
        $result->addOrder('entity_id');

        // Filter by title
        if ($title) {
            $result->addFieldToFilter('name', ['like' => '%' . $title . '%']);
        }

        // Page
        $result->setCurPage($page);

        // Limit
        $result->setPageSize($limit);

        $results = [];
        if ($result->getSize()) {
            foreach ($result as $item) {
                $results[] = $this->formatCollection($item);
            }
        }

        return $results;
    }

    /**
     * Count collection
     * @return Count
     */
    public function count()
    {
        $storeId = $this->settingHelper->getCurrentStoreId();
        $result = $this->collectionFactory->create();
        $result->addFieldToFilter('path', ['neq' => '1/2']);
        $result->addIsActiveFilter();
        $result->setStoreId($storeId);

        return $this->dataModelFactory->create(Count::class, ['count' => $result->getSize()]);
    }
}
