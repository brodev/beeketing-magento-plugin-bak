<?php

namespace BeeketingConnect\Platforms\Magento\Data;

use BeeketingConnect\Common\Data\CommonHelper;
use \Magento\Customer\Model\Customer as MagentoCustomer;
use BeeketingConnect\Common\Data\Factory;
use BeeketingConnect\Common\Data\Manager\CustomerManagerAbstract;
use BeeketingConnect\Common\Data\Model\Count;
use BeeketingConnect\Common\Data\Model\Customer;
use BeeketingConnect\Common\Data\Model\ResourceParams;
use BeeketingConnect\Platforms\Magento\Helper\Setting;

class CustomerManager extends CustomerManagerAbstract
{
    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    private $customerFactory;

    /**
     * @var \Magento\Newsletter\Model\Subscriber
     */
    private $subscriber;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    private $countryFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * @var Setting
     */
    private $settingHelper;

    /**
     * @var Factory
     */
    private $dataModelFactory;

    /**
     * CustomerManager constructor.
     * @param \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory
     * @param \Magento\Newsletter\Model\Subscriber $subscriber
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param Setting $settingHelper
     * @param Factory $dataModelFactory
     */
    public function __construct(
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Magento\Newsletter\Model\Subscriber $subscriber,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        Setting $settingHelper,
        Factory $dataModelFactory
    ) {
        $this->customerFactory = $customerCollectionFactory;
        $this->subscriber = $subscriber;
        $this->countryFactory = $countryFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->settingHelper = $settingHelper;
        $this->dataModelFactory = $dataModelFactory;
    }

    /**
     * Format customer
     *
     * @param \Magento\Customer\Model\Customer $customer
     * @return Customer
     */
    public function formatCustomer(MagentoCustomer $customer)
    {
        // Get accept marketing
        $acceptMarketing = false;
        $subscriber = $this->subscriber->loadByCustomerId($customer->getId());
        if ($subscriber->isSubscribed()) {
            $acceptMarketing = true;
        }

        // Get customer address
        $address = $this->getCustomerAddress($customer);

        $street1 = $street2 = $city = $company = $region = $postcode = $country = $countryCode = '';
        if ($address) {
            $street1 = $address->getStreetLine(1);
            $street2 = $address->getStreetLine(2);
            $city = $address->getCity();
            $company = $address->getCompany();
            $region = $address->getRegion();
            $postcode = $address->getPostcode();
            $countryCode = $address->getCountryId();
            $country = $this->countryFactory->create()->loadByCode($countryCode);
            $country = $country->getName();
        }

        // Get customer order
        $orders = $this->getOrdersByCustomer($customer);

        return $this->dataModelFactory->create(Customer::class, [
            'id' => (int)$customer->getId(),
            'email' => $customer->getEmail(),
            'first_name' => $customer->getFirstname(),
            'last_name' => $customer->getLastname(),
            'accepts_marketing' => $acceptMarketing,
            'verified_email' => $customer->isConfirmationRequired() ? $customer->getConfirmation() : true,
            'signed_up_at' => CommonHelper::formatDate($customer->getCreatedAt()),
            'address1' => $street1,
            'address2' => $street2,
            'city' => $city,
            'company' => $company,
            'province' => $region,
            'zip' => $postcode,
            'country' => $country,
            'country_code' => $countryCode,
            'orders_count' => $orders->getSize(),
            'total_spent' => $this->getTotalSpent($orders),
        ]);
    }

    /**
     * @param \Magento\Sales\Model\ResourceModel\Order\Collection $orders
     * @return string
     */
    private function getTotalSpent($orders)
    {
        // Get total spent
        $totalSpent = 0;
        foreach ($orders as $order) {
            $totalSpent += (float)$order->getSubtotal();
        }
        return number_format($totalSpent, 2);
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     * @return \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    private function getOrdersByCustomer($customer)
    {
        $orders = $this->orderCollectionFactory->create();
        $orders->addFieldToSelect('subtotal');
        $orders->addFieldToFilter('customer_id', $customer->getId());
        return $orders;
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     * @return mixed
     */
    private function getCustomerAddress($customer)
    {
        $address = $customer->getDefaultBillingAddress();
        if (!$address) {
            foreach ($customer->getAddresses() as $addr) {
                $address = $addr;
                break;
            }
        }
        return $address;
    }

    /**
     * Get a customer
     * @param $arg
     * @return Customer
     */
    public function get($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);
        $result = $this->customerFactory->create();
        $result->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', $params->getResourceId())
            ->setPageSize(1)
            ->setCurPage(1);

        if ($result->getSize()) {
            return $this->formatCustomer($result->getFirstItem());
        }

        return [];
    }

    /**
     * Get many customers
     *
     * @param $arg
     * @return array
     */
    public function getMany($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);
        $page = $params->getPage();
        $limit = $params->getLimit();

        $storeId = $this->settingHelper->getCurrentStoreId();
        $result = $this->customerFactory->create();
        $result->addAttributeToSelect('*');
        $result->addFieldToFilter('store_id', $storeId);
        $result->addOrder('entity_id');

        // Page
        $result->setCurPage($page);

        // Limit
        $result->setPageSize($limit);

        $results = [];
        if ($result->getSize()) {
            foreach ($result as $item) {
                $results[] = $this->formatCustomer($item);
            }
        }

        return $results;
    }

    /**
     * Count customers
     *
     * @return Count
     */
    public function count()
    {
        $storeId = $this->settingHelper->getCurrentStoreId();
        $result = $this->customerFactory->create();
        $result->addFieldToFilter('store_id', $storeId);

        return $this->dataModelFactory->create(Count::class, ['count' => $result->getSize()]);
    }
}
