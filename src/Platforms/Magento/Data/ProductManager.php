<?php

namespace BeeketingConnect\Platforms\Magento\Data;

use BeeketingConnect\Common\Data\CommonHelper;
use \Magento\Catalog\Model\Product as MagentoProduct;
use BeeketingConnect\Common\Constants as CommonConstants;
use BeeketingConnect\Common\Data\Factory;
use BeeketingConnect\Common\Data\Manager\ProductManagerAbstract;
use BeeketingConnect\Common\Data\Model\Count;
use BeeketingConnect\Common\Data\Model\Product;
use BeeketingConnect\Common\Data\Model\ResourceParams;
use BeeketingConnect\Platforms\Magento\Constants;
use BeeketingConnect\Platforms\Magento\Helper\Setting;

class ProductManager extends ProductManagerAbstract
{

    /**
     * @var VariantManager
     */
    private $variantManager;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator
     */
    private $productUrlPathGenerator;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    private $visibility;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $imageHelper;

    /**
     * @var Setting
     */
    private $settingHelper;

    /**
     * @var Factory
     */
    private $dataModelFactory;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * ProductManager constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator $productUrlPathGenerator
     * @param MagentoProduct\Visibility $visibility
     * @param \Magento\Catalog\Helper\Image $imageHelp
     * @param VariantManager $variantManager
     * @param Setting $settingHelper
     * @param Factory $dataModelFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator $productUrlPathGenerator,
        \Magento\Catalog\Model\Product\Visibility $visibility,
        \Magento\Catalog\Helper\Image $imageHelp,
        VariantManager $variantManager,
        Setting $settingHelper,
        Factory $dataModelFactory
    ) {
        $this->variantManager = $variantManager;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->stockRegistry = $stockRegistry;
        $this->resourceConnection = $resourceConnection;
        $this->productUrlPathGenerator = $productUrlPathGenerator;
        $this->visibility = $visibility;
        $this->imageHelper = $imageHelp;
        $this->settingHelper = $settingHelper;
        $this->dataModelFactory = $dataModelFactory;
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private function getConnection()
    {
        return $this->resourceConnection->getConnection();
    }

    /**
     * @param $name
     * @return string
     */
    private function getTableName($name)
    {
        return $this->resourceConnection->getTableName($name);
    }

    /**
     * Format product
     * @param MagentoProduct $product
     * @return Product
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function formatProduct(MagentoProduct $product)
    {
        // @codingStandardsIgnoreStart
        // Get images
        $productIds = $product->getTypeInstance()->getChildrenIds($product->getId());
        $productIds = array_shift($productIds) ?: $productIds;
        array_push($productIds, $product->getId());

        $select = $this->getConnection()->select()
            ->from(['cpgv' => $this->getTableName('catalog_product_entity_media_gallery_value')], '')
            ->join(['cpg' => $this->getTableName('catalog_product_entity_media_gallery')], 'cpgv.value_id = cpg.value_id')
            ->where('cpgv.entity_id IN (?)', $productIds)
            ->where('cpgv.disabled = ?', 0);

        $result = $this->getConnection()->fetchAll($select);
        $images = [];
        foreach ($result as $item) {
            // Resize image
            $imageUrl = $this->getResizeImageUrl($product, $item['value']);

            $images[] = [
                'id' => (int)$item['value_id'],
                'src' => $imageUrl,
            ];
        }

        // Get variants
        $variants = $this->variantManager->getVariantsByProduct($product);

        $options = [];
        $isValidOptions = true;
        if ($product->getHasOptions()) {
            $attributeOptions = $this->getProductAttributeOptions($product);
            $options[$product->getId()] =
                $this->formatListOption($attributeOptions, $product->getId());

        }

        // Resize image
        $imageUrl = $this->getResizeImageUrl($product, $product->getImage());

        $productStock = $this->stockRegistry->getStockStatus($product->getId());
        $stockStatus = $productStock->getStockStatus()
            ? CommonConstants::STOCK_STATUS_IN_STOCK
            : CommonConstants::STOCK_STATUS_OUT_OF_STOCK;
        $stock = $this->stockRegistry->getStockItem($product->getId());
        $productType = $product->getTypeId();
        $beeProduct = $this->dataModelFactory->create(Product::class, [
            'id' => (int)$product->getId(),
            'published_at' => $product->isAvailable() && $isValidOptions
                ? CommonHelper::formatDate($product->getCreatedAt()) : '',
            'handle' => $this->productUrlPathGenerator->getUrlPathWithSuffix($product, null),
            'title' => $product->getName(),
            'vendor' => '',
            'tags' => '',
            'description' => $product->getDescription(),
            'images' => $images,
            'image' => $imageUrl,
            'options' => isset($options[$product->getId()]) ? $options[$product->getId()] : [],
            'variants' => $variants,
            'collection_ids' => $product->getCategoryIds() ? array_map('intval', $product->getCategoryIds()) : [],
            'stock_status' => $stockStatus,
            'type' => $productType,
            'is_downloadable' => $productType === 'downloadable',
            'is_virtual' => $productType === 'virtual',
            'inventory_management' => $stock->getManageStock()
        ]);

        // @codingStandardsIgnoreEnd
        return $beeProduct;
    }

    /**
     * Get resize image path
     * @param $product
     * @param $imagePath
     * @return string
     */
    private function getResizeImageUrl($product, $imagePath)
    {
        return $this->imageHelper->init($product, 'product_image_thumbnail')
            ->setImageFile($imagePath)
            ->resize(Constants::DEFAULT_IMAGE_WIDTH, Constants::DEFAULT_IMAGE_HEIGHT)
            ->getUrl();
    }

    /**
     * Get Product
     * @param $arg
     * @return array|Product
     */
    public function get($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);
        $result = $this->productCollectionFactory->create();
        $result->addAttributeToSelect('*')
            ->addFieldToFilter('type_id', ['nin' => ['bundle', 'grouped']])
            ->setVisibility($this->visibility->getVisibleInSiteIds())
            ->addIdFilter($params->getResourceId())
            ->setPageSize(1)
            ->setCurPage(1);

        if ($result->getSize()) {
            $product = $result->getFirstItem();
            return $this->formatProduct($product);
        }

        return [];
    }

    /**
     * Get product
     *
     * @param $arg
     * @return array|Product[]
     */
    public function getMany($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);
        $page = $params->getPage();
        $limit = $params->getLimit();
        $title = $params->getTitle();

        $storeId = $this->settingHelper->getCurrentStoreId();
        $result = $this->productCollectionFactory->create();
        $result->addFieldToFilter('type_id', ['nin' => ['bundle', 'grouped']]);
        $result->setVisibility($this->visibility->getVisibleInSiteIds());
        $result->addAttributeToSelect('*');
        $result->addStoreFilter($storeId);
        $result->addOrder('entity_id');

        // Filter by title
        if ($title) {
            $result->addFieldToFilter('name', ['like' => '%' . $title . '%']);
        }

        // Page
        $result->setCurPage($page);

        // Limit
        $result->setPageSize($limit);

        $results = [];
        if ($result->getSize()) {
            /** @var \Magento\Catalog\Model\Product $item */
            foreach ($result as $item) {
                $results[] = $this->formatProduct($item);
            }
        }

        return $results;
    }

    /**
     * Count products
     *
     * @return Count
     */
    public function count()
    {
        $storeId = $this->settingHelper->getCurrentStoreId();
        $result = $this->productCollectionFactory->create();
        $result->addFieldToFilter('type_id', ['nin' => ['bundle', 'grouped']]);
        $result->setVisibility($this->visibility->getVisibleInSiteIds());
        $result->addStoreFilter($storeId);

        return $this->dataModelFactory->create(Count::class, ['count' => $result->getSize()]);
    }

    // @codingStandardsIgnoreStart
    /**
     * Update product
     * @param $arg
     * @return Product
     */
    public function put($arg)
    {
    }
    // @codingStandardsIgnoreEnd

    /**
     * format List Option
     * @param $options
     * @param $productId
     *
     * @return array
     */
    public function formatListOption($options, $productId)
    {
        $available_attribute = [];
        $position            = 0;
        foreach ($options as $value => $option) {
            $position++;
            $available_attribute[] = $this->formatOption($option, $value, $productId, $position);
        }

        return $available_attribute;
    }

    /**
     * format Option
     * @param $option
     * @param $value
     * @param $productId
     * @param $position
     *
     * @return mixed
     */
    public function formatOption($option, $value, $productId, $position)
    {
        $available_attribute['product_id'] = intval($productId);
        $available_attribute['name']       = $value;
        $available_attribute['position']   = $position;
        $available_attribute['values']     = array_values($option);

        return $available_attribute;
    }

    /**
     * get Product Attribute Options
     * @param $product
     *
     * @return array
     */
    public function getProductAttributeOptions($product)
    {
        $typeInstance = $product->getTypeInstance(true);
        if (!$typeInstance || !method_exists($typeInstance, 'getConfigurableAttributesAsArray')
        ) {
            return [];
        }
        $productAttributeOptions = $typeInstance->getConfigurableAttributesAsArray($product);
        $attributeOptions        = [];

        foreach ($productAttributeOptions as $productAttribute) {
            foreach ($productAttribute['values'] as $attribute) {
                $attributeOptions[$productAttribute['label']][$attribute['value_index']]
                    = $attribute['store_label'];
            }
        }

        return $attributeOptions;
    }
}
