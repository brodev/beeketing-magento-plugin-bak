<?php

namespace BeeketingConnect\Platforms\Magento\Data;

use BeeketingConnect\Common\Data\CommonHelper;
use \Magento\Catalog\Model\Product as MagentoProduct;
use \Magento\Catalog\Model\Product\Visibility;
use BeeketingConnect\Common\Data\Factory;
use BeeketingConnect\Common\Data\Manager\VariantManagerAbstract;
use BeeketingConnect\Common\Data\Model\ResourceParams;
use BeeketingConnect\Common\Data\Model\Variant;
use BeeketingConnect\Platforms\Magento\Helper\Common as Helper;

class VariantManager extends VariantManagerAbstract
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    private $configurable;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    private $productFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Option
     */
    private $productOptionFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Option\Value
     */
    private $productOptionValueFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var Factory
     */
    private $dataModelFactory;

    /**
     * VariantManager constructor.
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableProduct
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     * @param MagentoProduct $productFactory
     * @param MagentoProduct\Option $productOptionFactory
     * @param MagentoProduct\Option\Value $productOptionValueFactory
     * @param \Magento\Framework\Registry $registry
     * @param Factory $dataModelFactory
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableProduct,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Catalog\Model\Product $productFactory,
        \Magento\Catalog\Model\Product\Option $productOptionFactory,
        \Magento\Catalog\Model\Product\Option\Value $productOptionValueFactory,
        \Magento\Framework\Registry $registry,
        Factory $dataModelFactory
    ) {

        $this->productRepository = $productRepository;
        $this->stockRegistry = $stockRegistry;
        $this->configurable = $configurableProduct;
        $this->productCollectionFactory = $collectionFactory;
        $this->productFactory = $productFactory;
        $this->productOptionFactory = $productOptionFactory;
        $this->productOptionValueFactory = $productOptionValueFactory;
        $this->registry = $registry;
        $this->dataModelFactory = $dataModelFactory;
    }

    /**
     * @param MagentoProduct $product
     * @return array
     */
    public function getVariantsByProduct(MagentoProduct $product)
    {
        $variants = [];

        $productTypeInstance = $product->getTypeInstance();
        if ($productTypeInstance->getChildrenIds($product->getId())) {
            $usedProducts = $productTypeInstance->getUsedProducts($product);
        } else {
            $usedProducts[] = $product;
        }

        foreach ($usedProducts as $variant) {
            $variants[] = $this->formatVariant($variant, $product);
        }

        return $variants;
    }

    /**
     * Format variant
     *
     * @param \Magento\Catalog\Model\Product $variant
     * @param \Magento\Catalog\Model\Product|null $product
     * @return Variant
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function formatVariant(MagentoProduct $variant, MagentoProduct $product = null)
    {
        // If beeketing variant
        if (strpos($variant->getSku(), '_BEEKETING-') !== false) {
            preg_match('/(_BEEKETING-)(\d+)-(\d+)$/', $variant->getSku(), $skuMatches);
            if (isset($skuMatches[3]) && is_numeric($skuMatches[3])) {
                $product = $this->productRepository->getById($skuMatches[3]);
            }
        }

        if ($product) {
            $productId = $product->getId();
        } else {
            $productIds = $this->configurable->getParentIdsByChild($variant->getId());
            $productId = array_shift($productIds);
            $result = $this->productCollectionFactory->create();
            $product = $result->addAttributeToSelect('*')
                ->addIdFilter($productId)
                ->setPageSize(1)
                ->setCurPage(1)
                ->getFirstItem();
        }

        $stock = $this->stockRegistry->getStockItem($variant->getId());
        $variantImage = $variant->getMediaGalleryImages()
            ? $variant->getMediaGalleryImages()
                ->setPageSize(1)
                ->setCurPage(1)
                ->getFirstItem()
            : '';

        $beeVariant = $this->dataModelFactory->create(Variant::class, [
            'id' => (int)$variant->getId(),
            'product_id' => (int)$productId,
            'barcode' => '',
            'image_id' => $variantImage ? (int)$variantImage->getId() : '',
            'title' => $variant->getName(),
            'price' => Helper::formatPrice($variant->getFinalPrice()),
            'price_compare' => $variant->getPrice() > $variant->getFinalPrice()
                ? Helper::formatPrice($variant->getPrice())
                : '',
            'option1' => $variant->getName(),
            'option2' => '',
            'option3' => '',
            'grams' => '',
            'position' => '',
            'sku' => $variant->getSku(),
            'inventory_management' => $stock->getManageStock(),
            'inventory_policy' => "",
            'inventory_quantity' => $stock->getQty(),
            'fulfillment_service' => '',
            'weight' => $variant->getWeight(),
            'weight_unit' => '',
            'requires_shipping' => '',
            'taxable' => false,
            'updated_at' => CommonHelper::formatDate($variant->getUpdatedAt()),
            'created_at' => CommonHelper::formatDate($variant->getCreatedAt()),
            'in_stock' => $stock->getIsInStock(),
            'attributes' => $this->getProductAttributes($variant, $product),
        ]);

        return $beeVariant;
    }

    /**
     * @param MagentoProduct $variant
     * @param MagentoProduct $product
     * @return array
     */
    private function getProductAttributes(MagentoProduct $variant, MagentoProduct $product)
    {
        $attributes = [];
        $productTypeInstance = $product->getTypeInstance();
        if ($productTypeInstance->getChildrenIds($product->getId())) {
            $superAttributes = $productTypeInstance->getConfigurableAttributesAsArray($product);
            foreach ($superAttributes as $id => $attribute) {
                if ($attributeData = $variant->getCustomAttribute($attribute['attribute_code'])) {
                    $attributes[$id] = $attributeData->getValue();
                }
            }
        }
        return $attributes;
    }

    /**
     * @param $arg
     * @return Variant|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);
        if (!$params->getProductId() || !$params->getVariantId()) {
            return null;
        }

        $product = $this->productRepository->getById($params->getProductId());

        // Check product is existed
        if (!$product) {
            return null;
        }

        // Check variant is valid with product
        if (!$this->isValidProductVariants($product, $params->getVariantId())) {
            return null;
        }

        if ($variant = $this->productRepository->getById($params->getVariantId())) {
            return $this->formatVariant($variant, $product);
        }

        return [];
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param $variantId
     * @return bool
     */
    private function isValidProductVariants(MagentoProduct $product, $variantId)
    {
        // Get list variant of product
        $productTypeInstance = $product->getTypeInstance();
        $variantList = $productTypeInstance->getChildrenIds($product->getId());

        if (!$variantList) {
            return false;
        }

        foreach ($variantList as $list) {
            if (isset($list[$variantId])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Update variant
     *
     * @param $arg
     * @return Variant
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function put($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);

        /** @var \Magento\Catalog\Model\Product $variant */
        $variant = $this->productRepository->getById($params->getVariantId());

        if ($params->getParamByFieldName('price', false)) {
            $variant->setPrice($params->getParamByFieldName('price'));
        }

        $variant->getResource()->save($variant);

        $stock = $this->stockRegistry->getStockItem($variant->getId());
        $stock->setQty($params->getParamByFieldName('inventory_quantity', 0));
        $this->stockRegistry->updateStockItemBySku($variant->getSku(), $stock);

        $product = $this->productRepository->getById($params->getProductId());

        return $this->formatVariant($variant, $product);
    }

    /**
     * Create new variant for product
     *
     * @param $arg
     * @return Variant
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function post($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);
        $productId = $params->getProductId();
        $originId = $params->getParamByFieldName('origin_id');

        $product = $this->productRepository->getById($productId);
        $variant = $this->productRepository->getById($originId);

        $originStock = $this->stockRegistry->getStockItem($variant->getId());
        $originSku = $variant->getSku();
        $websiteIds = $variant->getWebsiteIds();
        $variant->setId(null);
        $variant->setVisibility(Visibility::VISIBILITY_NOT_VISIBLE);

        // Get sku
        preg_match('/(\(.*)\s(\d+)(\))$/', $params->getParamByFieldName('option1'), $skuMatches);
        $skuPostfix = '_BEEKETING-';
        if (isset($skuMatches[2]) && is_numeric($skuMatches[2]) && $skuMatches[2]) {
            $skuPostfix .= $skuMatches[2];
        } else {
            $skuPostfix .= '0' . substr(hexdec(uniqid()), -5, 5);
        }
        $skuPostfix .= '-' . $productId;

        $variant->setSku($variant->getSku() . $skuPostfix);
        $variant->setData('image', null);
        $variant->setData('media_gallery', null);
        $variant->setName($params->getParamByFieldName('option1'));
        $variant->setPrice($params->getParamByFieldName('price'));

        $newVariant = $this->productFactory;
        $newVariant->setData($variant->getData());
        $newVariant->setStockData([]);
        $newVariant->setWebsiteIds($websiteIds);

        $options = [];
        if ($product->getHasOptions()) {
            foreach ($product->getOptions() as $productOption) {
                if ($productOption->getValues()) {
                    $arrayValues = [];
                    foreach ($productOption->getValues() as $value) {
                        $value->setId(null);
                        $optionValue = $this->productOptionValueFactory
                            ->setData($value->getData());
                        $arrayValues[] = $optionValue;
                    }

                    $productOption->setId(null);
                    $productOption->setProduct($newVariant);
                    $option = $this->productOptionFactory
                        ->setData($productOption->getData())
                        ->setProductId($newVariant->getId())
                        ->setStoreId($newVariant->getStoreId())
                        ->setProductSku($productOption->getProductSku())
                        ->setValues($arrayValues);
                    $options[] = $option;
                }
            }

            if ($options) {
                $newVariant->setHasOptions(1);
                $newVariant->setCanSaveCustomOptions(true);
            }

            $newVariant->setOptions($options);
        }

        $newVariant->getResource()->save($newVariant);

        // Update origin stock
        $this->stockRegistry->updateStockItemBySku($originSku, $originStock);

        // Update new variant stock
        $stock = $this->stockRegistry->getStockItem($newVariant->getId());
        $stock->setUseConfigManageStock(false);
        $stock->setManageStock(false);
        if ($params->getParamByFieldName('inventory_quantity', false)) {
            $stock->setQty($params->getParamByFieldName('inventory_quantity'));
        }
        $this->stockRegistry->updateStockItemBySku($newVariant->getSku(), $stock);

        return $this->formatVariant($newVariant, $newVariant);
    }

    /**
     * Delete variant
     *
     * @param $arg
     * @return bool|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);
        $registry = $this->registry;
        $registry->register('isSecureArea', true);

        $product = $this->productRepository->getById($params->getVariantId());
        $this->productRepository->delete($product);
    }
}
