<?php

namespace BeeketingConnect\Platforms\Magento\Data;

use BeeketingConnect\Common\Data\Factory;
use BeeketingConnect\Common\Data\Manager\ShopManagerAbstract;
use BeeketingConnect\Common\Data\Model\Shop;
use BeeketingConnect\Platforms\Magento\Helper\Common;
use \Magento\Framework\Pricing\PriceCurrencyInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;

class ShopManager extends ShopManagerAbstract
{

    /**
     * @var Common
     */
    private $commonHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var \Magento\Store\Model\Information
     */
    private $storeInfo;

    /**
     * @var Factory
     */
    private $dataModelFactory;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * ShopManager constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Store\Model\Information $storeInfo
     * @param Common $commonHelper
     * @param Factory $dataModelFactory
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Store\Model\Information $storeInfo,
        Common $commonHelper,
        Factory $dataModelFactory,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->commonHelper = $commonHelper;
        $this->storeManager = $storeManager;
        $this->priceCurrency = $priceCurrency;
        $this->storeInfo = $storeInfo;
        $this->dataModelFactory = $dataModelFactory;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return Shop
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get()
    {
        $store = $this->storeManager->getStore();

        $baseCurrency = $store->getBaseCurrency()->getCode();
        $formattedPrice = $this->formatPrice($baseCurrency);

        $storeInfoObject = $this->storeInfo->getStoreInformationObject($store);
        $address = empty($storeInfoObject->getDataByKey('street_line1')) ?
            $storeInfoObject->getDataByKey('street_line2')
            : $storeInfoObject->getDataByKey('street_line1');
        $countryId = $this->scopeConfig->getValue('general/store_information/country_id');
        return $this->dataModelFactory->create(Shop::class, [
            'domain' => $this->commonHelper->getShopDomain(),
            'absolute_path' => $this->commonHelper->getShopAbsolutePath(),
            'currency' => $baseCurrency,
            'currency_format' => $this->getFormatCurrency($formattedPrice),
            'name' => $storeInfoObject->getName(),
            'id' => $store->getId(),
            'plugin_version' => BEEKETINGMAGENTO_VERSION,
            'address' => $address,
            'country' => $countryId,
            'zip' => $storeInfoObject->getPostcode(),
        ]);
    }

    public function getFormatCurrency($formattedPrice)
    {
        return preg_replace(
            '/[1]+[.,]{0,1}[1]+/',
            '{{amount}}',
            $formattedPrice,
            1
        );
    }

    public function formatPrice($baseCurrency)
    {
        return $this->priceCurrency->format(
            '11.11',
            true,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            null,
            $baseCurrency
        );
    }
}
