<?php

namespace BeeketingConnect\Platforms\Magento\Data;

use BeeketingConnect\Common\Data\Factory;
use BeeketingConnect\Common\Data\Manager\CartManagerAbstract;
use BeeketingConnect\Common\Data\Model\Cart;
use BeeketingConnect\Common\Data\Model\CartItem;
use BeeketingConnect\Common\Data\Model\ResourceParams;

class CartManager extends CartManagerAbstract
{
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cart;

    /**
     * @var \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator
     */
    private $productUrlPathGenerator;

    /**
     * @var \Magento\Quote\Model\ResourceModel\Quote\Item
     */
    private $quoteItem;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $imageHelper;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    private $localeResolver;

    /**
     * @var Factory
     */
    private $dataModelFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * CartManager constructor.
     * @param \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator $productUrlPathGenerator
     * @param \Magento\Quote\Model\ResourceModel\Quote\Item $quoteItem
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Locale\ResolverInterface $localeResolver
     * @param Factory $dataModelFactory
     */
    public function __construct(
        \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator $productUrlPathGenerator,
        \Magento\Quote\Model\ResourceModel\Quote\Item $quoteItem,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        Factory $dataModelFactory
    ) {
        $this->cart = $cart;
        $this->productUrlPathGenerator = $productUrlPathGenerator;
        $this->quoteItem = $quoteItem;
        $this->imageHelper = $imageHelper;
        $this->dataModelFactory = $dataModelFactory;
        $this->productRepository = $productRepository;
        $this->localeResolver = $localeResolver;
    }

    /**
     * Save cart
     */
    public function saveCart()
    {
        $this->cart->save();
    }

    /**
     * Format item
     *
     * @param $variant
     * @param $item
     * @return CartItem
     */
    private function formatItem($variant, $item)
    {
        $product = $variant->getProduct() ?: $item->getProduct();

        $productTypeInstance = $product->getTypeInstance();
        $options = $productTypeInstance->getOrderOptions($product);
        $infoBuyRequestOptions = isset($options['options']) ? $options['options'] : [];

        $variantId = $variant->getProductId();
        $variantNames[] = $variant->getName();
        if ($infoBuyRequestOptions) {
            foreach ($infoBuyRequestOptions as $infoBuyRequestOption) {
                $variantNames[] = $infoBuyRequestOption['value'];
            }
        }

        $productId = $item->getProductId();
        if (strpos($item->getSku(), '_BEEKETING-') !== false) {
            preg_match('/(_BEEKETING-)(\d+)-(\d+)$/', $variant->getSku(), $skuMatches);
            if (isset($skuMatches[3]) && is_numeric($skuMatches[3])) {
                $productId = $skuMatches[3];
            }
        }

        // Get resize image
        $imageUrl = $this->imageHelper->init($product, 'product_image_thumbnail')
            ->setImageFile($product->getData('thumbnail'))
            ->resize(240, 300)
            ->getUrl();

        return $this->dataModelFactory->create(CartItem::class, [
            'id' => (int)$item->getId(),
            'title' => implode('/', $variantNames),
            'price' => (float)$item->getPrice(),
            'sku' => $variant->getSku(),
            'variant_id' => (int)$variantId,
            'variant_title' => implode('/', $variantNames),
            'product_id' => (int)$productId,
            'product_title' => $item->getName(),
            'line_price' => (float)$item->getPrice() * (int)$item->getQty(),
            'quantity' => (int)$item->getQty(),
            'handle' => $this->productUrlPathGenerator->getUrlPathWithSuffix($item->getProduct(), null),
            'image' => $imageUrl,
            'url' => $item->getProduct()->getProductUrl(),
        ]);
    }

    /**
     * Get cart
     * @param $useCookie
     * @return Cart
     */
    public function get($useCookie = true)
    {
        $cart = $this->cart->getQuote();

        $cartData = [
            'token' => $cart->getId(),
            'item_count' => (int)$cart->getItemsCount(),
            'total_price' => (float)$cart->getBaseGrandTotal(),
            'subtotal_price' => $cart->getBaseSubtotal(),
            'items' => [],
        ];

        // Traverse cart items
        $variantItems = [];
        foreach ($cart->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                $variantItems[$item->getParentItemId()] = $item;
            }
        }

        foreach ($cart->getAllVisibleItems() as $item) {
            $variant = isset($variantItems[$item->getId()]) ? $variantItems[$item->getId()] : $item;
            $cartData['items'][] = $this->formatItem($variant, $item);
        }

        return $this->dataModelFactory->create(Cart::class, $cartData);
    }

    // @codingStandardsIgnoreStart
    /**
     * @param $arg
     * @return array|\BeeketingConnect\Common\Data\Manager\CartItem|Cart|CartItem
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function post($arg)
    {
        $resourceParams = $this->dataModelFactory->create(ResourceParams::class, $arg);
        $type = $resourceParams->getType();
        $productId = $resourceParams->getProductId();
        $variantId = $resourceParams->getVariantId();
        $quantity = $resourceParams->getQuantity();
        $attributes = $resourceParams->getParamByFieldName('attributes', []);
        $options = $resourceParams->getParamByFieldName('options', []);

        if ($type == 'multi') { // Add multi
            foreach ($productId as $key => $id) {
                /** @var \Magento\Catalog\Model\Product $product */
                $product = $this->productRepository->getById($id);
                $productTypeInstance = $product->getTypeInstance();
                $childrenIds = $productTypeInstance->getChildrenIds($product->getId());
                $childrenIds = array_shift($childrenIds) ?: $childrenIds;
                $optionsItem = $options && isset($options[$key]) ? $options[$key] : [];

                // Get real product id
                if (isset($variantId[$key]) && !in_array($variantId[$key], $childrenIds)) {
                    $id = $variantId[$key];
                    $optionsItem = $this->getRealOptions($product, $optionsItem, $id);
                }

                $params = array(
                    'qty' => $quantity ? $quantity[$key] : 1,
                    'super_attribute' => $attributes && isset($attributes[$key]) ? $attributes[$key] : array(),
                    'options' => $optionsItem,
                );

                $this->addCart($id, $params, false);
            }

            $this->saveCart();
            $result = $this->get(true);
        } else { // Add single
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->productRepository->getById($productId);
            $productTypeInstance = $product->getTypeInstance();
            $childrenIds = $productTypeInstance->getChildrenIds($product->getId());
            $childrenIds = array_shift($childrenIds) ?: $childrenIds;

            // Get real product id
            if (isset($variantId) && !in_array($variantId, $childrenIds)) {
                $productId = $variantId;
                $options = $this->getRealOptions($product, $options, $productId);
            }

            $params = array(
                'qty' => $quantity,
                'super_attribute' => $attributes,
                'options' => $options,
            );

            $result = $this->addCart($productId, $params);
        }

        return $result;
    }

    /**
     * @param $product
     * @param $options
     * @param $productId
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getRealOptions($product, $options, $productId)
    {
        if ($options && $product->getHasOptions()) {
            $productOptionsArray = [];
            $productOptions = $product->getProductOptionsCollection();
            foreach ($productOptions as $option) {
                if ($option->getIsRequire()) {
                    $values = [];
                    foreach ($option->getValues() as $value) {
                        $values[$value->getOptionTypeId()] = [
                            'title' => $value->getTitle(),
                            'position' => $value->getSortOrder(),
                        ];
                    }

                    $productOptionsArray[$option->getOptionId()] = [
                        'title' => $option->getTitle(),
                        'position' => $option->getSortOrder(),
                        'values' => $values,
                    ];
                }
            }

            $optionsAdded = [];
            foreach ($options as $optionId => $optionTypeId) {
                if (isset($productOptionsArray[$optionId])) {
                    $productOptionAdded = $productOptionsArray[$optionId];
                    if (isset($productOptionAdded['values'][$optionTypeId])) {
                        $productOptionAddedValue = $productOptionAdded['values'][$optionTypeId];
                        $optionsAdded[] = [
                            'title' => $productOptionAdded['title'],
                            'position' => $productOptionAdded['position'],
                            'type_title' => $productOptionAddedValue['title'],
                            'type_position' => $productOptionAddedValue['position'],
                        ];
                    }
                }
            }

            $product = $this->productRepository->getById($productId);
            $realOptions = [];
            $productOptions = $product->getProductOptionsCollection();
            foreach ($productOptions as $option) {
                if ($option->getIsRequire()) {
                    foreach ($optionsAdded as $optionAdded) {
                        if ($optionAdded['title'] == $option->getTitle() && $optionAdded['position'] == $option->getSortOrder()) {
                            foreach ($option->getValues() as $value) {
                                if ($optionAdded['type_title'] == $value->getTitle() && $optionAdded['type_position'] == $value->getSortOrder()) {
                                    $realOptions[$option->getOptionId()] = $value->getOptionTypeId();
                                }

                                $values[$value->getOptionTypeId()] = [
                                    'title' => $value->getTitle(),
                                    'position' => $value->getSortOrder(),
                                ];
                            }
                        }
                    }
                }
            }

            if ($realOptions) {
                $options = $realOptions;
            }
        }

        return $options;
    }


    /**
     * @param $productId
     * @param $params
     * @param bool $save
     * @return array|CartItem
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addCart($productId, $params, $save = true)
    {
        // Add product to cart
        $this->cart->addProduct($productId, $params);

        if ($save) {
            $this->cart->save();

            // Get added item
            $cart = $this->cart->getQuote();

            // Traverse cart items
            $variantItems = [];
            foreach ($cart->getAllItems() as $item) {
                if ($item->getParentItemId()) {
                    $variantItems[$item->getParentItemId()] = $item;
                }
            }

            // Traverse cart visible items
            foreach ($cart->getAllVisibleItems() as $item) {
                if ($item->getProductId() != $productId) {
                    continue;
                }

                $variant = isset($variantItems[$item->getId()]) ? $variantItems[$item->getId()] : $item;

                // Check attributes
                $vResult = null;
                $product = $item->getProduct();
                $productTypeInstance = $product->getTypeInstance();
                if (isset($params['super_attribute']) &&
                    $params['super_attribute'] &&
                    $productTypeInstance->getChildrenIds($product->getId())
                ) {
                    $options = $productTypeInstance->getOrderOptions($product);
                    $infoBuyRequestSuperAttribute = isset($options['info_buyRequest']['super_attribute']) ?
                        $options['info_buyRequest']['super_attribute'] : [];
                    $count = 0;
                    if ($infoBuyRequestSuperAttribute) {
                        foreach ($params['super_attribute'] as $supperAttributeId => $supperAttributeValue) {
                            foreach ($infoBuyRequestSuperAttribute as $k => $v) {
                                if ($k == $supperAttributeId && $v == $supperAttributeValue) {
                                    $count++;
                                }
                            }
                        }
                    }

                    // Validate
                    if ($count != count($infoBuyRequestSuperAttribute)) {
                        continue;
                    }
                }

                // Check options
                if (isset($params['options']) && $params['options']) {
                    $options = $productTypeInstance->getOrderOptions($product);
                    $infoBuyRequestOptions = isset($options['info_buyRequest']['options']) ?
                        $options['info_buyRequest']['options'] : [];
                    $count = 0;
                    if ($infoBuyRequestOptions) {
                        foreach ($params['options'] as $optionId => $optionValue) {
                            foreach ($infoBuyRequestOptions as $k => $v) {
                                if ($k == $optionId && $v == $optionValue) {
                                    $count++;
                                }
                            }
                        }
                    }

                    // Validate
                    if ($count != count($infoBuyRequestOptions)) {
                        continue;
                    }
                }

                return $this->formatItem($variant, $item);
            }
        }

        return [];
    }

    /**
     * Update cart quantity
     * @param $arg
     * @return Cart
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function put($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);
        if ($params->getQuantity()) { // Update cart item
            $cartData[$params->getItemId()]['qty'] = $params->getQuantity();
            if (is_array($cartData)) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->localeResolver->getLocale()]
                );
                foreach ($cartData as $index => $data) {
                    if (isset($data['qty'])) {
                        $cartData[$index]['qty'] = $filter->filter(trim($data['qty']));
                    }
                }
                if (!$this->cart->getCustomerSession()->getCustomerId() && $this->cart->getQuote()->getCustomerId()) {
                    $this->cart->getQuote()->setCustomerId(null);
                }
                $cartData = $this->cart->suggestItemsQty($cartData);
                $this->cart->updateItems($cartData);
            }
        } else { // Remove cart item
            $this->cart->removeItem($params->getProductId());
        }

        return $this->get(true);
    }
    // @codingStandardsIgnoreEnd
}
