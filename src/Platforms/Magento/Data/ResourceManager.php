<?php

namespace BeeketingConnect\Platforms\Magento\Data;

use BeeketingConnect\Common\Data\Manager\ResourceManagerAbstract;

class ResourceManager extends ResourceManagerAbstract
{
    /**
     * ResourceManager constructor.
     * @param CartManager $cartManager
     * @param CollectionManager $collectionManager
     * @param CollectManager $collectManager
     * @param OrderManager $orderManager
     * @param ProductManager $productManager
     * @param VariantManager $variantManager
     * @param ShopManager $shopManager
     * @param CustomerManager $customerManager
     * @param ImageManager $imageManager
     */
    public function __construct(
        CartManager $cartManager,
        CollectionManager $collectionManager,
        CollectManager $collectManager,
        OrderManager $orderManager,
        ProductManager $productManager,
        VariantManager $variantManager,
        ShopManager $shopManager,
        CustomerManager $customerManager,
        ImageManager $imageManager
    ) {
    
        parent::__construct(
            $cartManager,
            $collectionManager,
            $collectManager,
            $customerManager,
            $orderManager,
            $productManager,
            $shopManager,
            $variantManager,
            $imageManager
        );
    }
}
