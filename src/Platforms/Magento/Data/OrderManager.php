<?php

namespace BeeketingConnect\Platforms\Magento\Data;

use BeeketingConnect\Common\Data\CommonHelper;
use BeeketingConnect\Common\Data\Model\OrderItem;
use \Magento\Sales\Model\Order as MagentoOrder;
use BeeketingConnect\Common\Data\Factory;
use BeeketingConnect\Platforms\Magento\Helper\Setting;
use BeeketingConnect\Common\Data\Manager\OrderManagerAbstract;
use BeeketingConnect\Common\Data\Model\Count;
use BeeketingConnect\Common\Data\Model\Customer;
use BeeketingConnect\Common\Data\Model\Order;
use BeeketingConnect\Common\Data\Model\ResourceParams;
use BeeketingConnect\Platforms\Magento\Helper\Setting as SettingHelper;

class OrderManager extends OrderManagerAbstract
{
    /**
     * @var \BeeketingConnect\Common\Data\Manager\CustomerManagerAbstract
     */
    private $customerManager;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    private $countryFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Setting
     */
    private $settingHelper;

    /**
     * @var Factory
     */
    private $dataModelFactory;

    /**
     * OrderManager constructor.
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory
     * @param CustomerManager $customerManager
     * @param SettingHelper $settingHelper
     * @param Factory $dataModelFactory
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        CustomerManager $customerManager,
        Setting $settingHelper,
        Factory $dataModelFactory
    ) {

        $this->customerManager = $customerManager;
        $this->orderCollectionFactory = $collectionFactory;
        $this->countryFactory = $countryFactory;
        $this->productRepository = $productRepository;
        $this->settingHelper = $settingHelper;
        $this->dataModelFactory = $dataModelFactory;
    }

    /**
     * Format order
     *
     * @param \Magento\Sales\Model\Order $order
     * @return Order
     */
    public function formatOrder(MagentoOrder $order)
    {
        $beeOrderData = [
            'id' => (int)$order->getId(),
            'email' => $order->getCustomerEmail(),
            'financial_status' => $order->getStatus(),
            'fulfillment_status' => '',
            'line_items' => [],
            'cart_token' => $order->getQuoteId(),
            'currency' => $order->getBaseCurrencyCode(),
            'name' => $order->getRealOrderId(),

            'total_tax' => $order->getBaseTaxAmount(),
            'total_discounts' => $order->getBaseDiscountAmount(),
            'total_price' => $order->getBaseGrandTotal(),
            'subtotal_price' => $order->getBaseSubtotal(),
            'total_line_items_price' => $order->getBaseSubtotal(),

            'processed_at' => CommonHelper::formatDate($order->getCreatedAt()),
            'updated_at' => CommonHelper::formatDate($order->getUpdatedAt()),
            'cancelled_at' => "",
            'note_attributes' => [],
            'source_name' => '',
            'billing_address' => []
        ];
        $address = $order->getBillingAddress();
        $countryCode = $address->getCountryId();
        $country = $this->countryFactory->create()->loadByCode($countryCode);

        // Add contact info
        if ($order->getCustomerId()) {
            $contact = $this->customerManager->get(['resource_id' => $order->getCustomerId()]);

            if ($contact) {
                $beeOrderData['customer'] = $contact;
            }
        } else {
            $customer = $this->dataModelFactory->create(Customer::class, [
                'id' => $order->getCustomerId(),
                'email' => $order->getCustomerEmail(),
                'first_name' => $order->getCustomerFirstname(),
                'last_name' => $order->getCustomerLastname(),
                'address1' => $address->getStreetLine(1),
                'address2' => $address->getStreetLine(2),
                'city' => $address->getCity(),
                'company' => $address->getCompany(),
                'province' => $address->getRegion(),
                'zip' => $address->getPostcode(),
                'country' => $country->getName(),
                'country_code' => $countryCode,
                'signed_up_at' => CommonHelper::formatDate($order->getCreatedAt()),
                'accepts_marketing' => false,
                'verified_email' => '',
                'orders_count' => '',
                'total_spent' => '',
            ]);

            $beeOrderData['customer'] = $customer;
        }

        // Add line items
        /** @var \Magento\Sales\Model\Order\Item $item */
        $variantItems = [];
        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItem()) {
                $variantItems[$item->getParentItem()->getQuoteItemId()] = $item;
            }
        }

        /** @var \Magento\Sales\Model\Order\Item $item */
        foreach ($order->getAllVisibleItems() as $item) {
            // Fix visible items webhook
            if ($item->getParentItem() && !$item->getId()) {
                continue;
            }

            $variant = isset($variantItems[$item->getQuoteItemId()]) ? $variantItems[$item->getQuoteItemId()] : $item;
            $productId = $item->getProductId();
            // If beeketing variant
            if (strpos($item->getSku(), '_BEEKETING-') !== false) {
                preg_match('/(_BEEKETING-)(\d+)-(\d+)$/', $item->getSku(), $skuMatches);
                if (isset($skuMatches[3]) && is_numeric($skuMatches[3])) {
                    $productId = $skuMatches[3];
                }
            }

            $beeOrderData['line_items'][] = $this->dataModelFactory->create(OrderItem::class, [
                'id' => (int)$item->getQuoteItemId(),
                'title' => $item->getName(),
                'price' => $item->getBasePrice(),
                'line_price' => $item->getBasePrice(),
                'sku' => $item->getSku(),
                'requires_shipping' => '',
                'taxable' => $item->getTaxAmount(),
                'product_id' => (int)$productId,
                'variant_id' => (int)$variant->getProductId(),
                'vendor' => '',
                'name' => $variant->getName(),
                'fulfillable_quantity' => (int)$item->getQtyOrdered(),
                'fulfillment_service' => '',
                'fulfillment_status' => '',
            ]);
        }

        $beeOrderData['billing_address'] = [
            "first_name" => $address->getFirstname(),
            "last_name" => $address->getLastname(),
            "address1" => $address->getStreetLine(1),
            "address2" => $address->getStreetLine(2),
            "city" => $address->getCity(),
            "country" => $country->getName(),
            "zip" => $address->getPostcode(),
        ];

        return $this->dataModelFactory->create(Order::class, $beeOrderData);
    }

    /**
     * Get order
     *
     * @param $arg
     * @return Order
     */
    public function get($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);
        $result = $this->orderCollectionFactory->create();
        $result->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', $params->getResourceId())
            ->setPageSize(1)
            ->setCurPage(1);

        if ($result->getSize()) {
            return $this->formatOrder($result->getFirstItem());
        }

        return null;
    }

    /**
     * Get many orders
     * @param $arg
     * @return array
     */
    public function getMany($arg)
    {
        $params = $this->dataModelFactory->create(ResourceParams::class, $arg);
        $page = $params->getPage();
        $limit = $params->getLimit();

        $storeId = $this->settingHelper->getCurrentStoreId();
        $result = $this->orderCollectionFactory->create();
        $result->addAttributeToSelect('*');
        $result->addFieldToFilter('store_id', $storeId);
        $result->addOrder('entity_id');

        if ($params->getStatus() && $params->getStatus() != 'any') {
            $result->addFieldToFilter('status', $params->getStatus());
        }

        // Page
        $result->setCurPage($page);

        // Limit
        $result->setPageSize($limit);

        $results = [];
        if ($result->getSize()) {
            foreach ($result as $item) {
                $results[] = $this->formatOrder($item);
            }
        }

        return $results;
    }

    /**
     * Count order by status
     *
     * @param $arg
     * @return Count
     */
    public function count($arg)
    {
        $storeId = $this->settingHelper->getCurrentStoreId();
        $result = $this->orderCollectionFactory->create();
        $result->addFieldToFilter('store_id', $storeId);

        return $this->dataModelFactory->create(Count::class, ['count' => $result->getSize()]);
    }
}
