<?php
/**
 * Observer catalog_category_change_products
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace BeeketingConnect\Platforms\Magento\Observer\Catalog;

use BeeketingConnect\Common\Webhook;

class CategoryChangeProducts implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \BeeketingConnect\Platforms\Magento\Core\Api\App
     */
    private $app;
    /**
     * @var \BeeketingConnect\Platforms\Magento\Data\CollectionManager
     */
    private $collectionManager;

    private $webhook;

    /**
     * CategoryChangeProducts constructor.
     * @param \BeeketingConnect\Platforms\Magento\Core\Api\App $app
     * @param \BeeketingConnect\Platforms\Magento\Data\CollectionManager $collectionManager
     * @param Webhook $webhook
     */
    public function __construct(
        \BeeketingConnect\Platforms\Magento\Core\Api\App $app,
        \BeeketingConnect\Platforms\Magento\Data\CollectionManager $collectionManager
    ) {
        $this->app = $app;
        $this->collectionManager = $collectionManager;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->app->init();
        /** @var \Magento\Catalog\Model\Category $collection */
        $collection = $observer->getCategory();

        // Set store scope
        $storeId = $collection->getStoreId();
        $this->app->getSettingHelper()->setStoreId($storeId);

        $content = $this->collectionManager->formatCollection($collection);
        $this->app->sendRequestWebhook(Webhook::COLLECTION_UPDATE, $content);
    }
}
