<?php
/**
 * Observer catalog_product_delete_after_done
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace BeeketingConnect\Platforms\Magento\Observer\Catalog;

use BeeketingConnect\Common\Webhook;
use BeeketingConnect\Platforms\Magento\Core\Api\App;

class ProductDeleteAfterDone implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \BeeketingConnect\Platforms\Magento\Core\Api\App
     */
    private $app;

    /**
     * ProductDeleteAfterDone constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->app->init();
        $product = $observer->getProduct();
        $this->app->sendRequestWebhook(Webhook::PRODUCT_DELETE, ['id' => $product->getId()]);
    }
}
