<?php
/**
 * Observer customer_save_after
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace BeeketingConnect\Platforms\Magento\Observer\Customer;

class SaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \BeeketingConnect\Platforms\Magento\Core\Api\App
     */
    private $app;
    /**
     * @var \BeeketingConnect\Platforms\Magento\Data\CustomerManager
     */
    private $customerManager;

    /**
     * Constructor.
     *
     * @param \BeeketingConnect\Platforms\Magento\Core\Api\App $app
     * @param \BeeketingConnect\Platforms\Magento\Data\CustomerManager $customerManager
     */
    public function __construct(
        \BeeketingConnect\Platforms\Magento\Core\Api\App $app,
        \BeeketingConnect\Platforms\Magento\Data\CustomerManager $customerManager
    ) {
        $this->app = $app;
        $this->customerManager = $customerManager;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->app->init();
        /** @var \Magento\Customer\Model\Customer $customer */
        $customer = $observer->getCustomer();

        // Set store scope
        $storeId = $customer->getStoreId();
        $this->app->getSettingHelper()->setStoreId($storeId);

        $content = $this->customerManager->formatCustomer($customer);
        $this->app->sendRequestWebhook(\BeeketingConnect\Common\Webhook::CUSTOMER_UPDATE, $content);
    }
}
