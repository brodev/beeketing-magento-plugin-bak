<?php
/**
 * Observer customer_delete_after
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace BeeketingConnect\Platforms\Magento\Observer\Customer;

class DeleteAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \BeeketingConnect\Platforms\Magento\Core\Api\App
     */
    private $app;
    /**
     * @var \BeeketingConnect\Platforms\Magento\Data\CustomerManager
     */
    private $customerManager;

    /**
     * DeleteAfter constructor.
     * @param \BeeketingConnect\Platforms\Magento\Core\Api\App $app
     * @param \BeeketingConnect\Platforms\Magento\Data\CustomerManager $customerManager
     */
    public function __construct(
        \BeeketingConnect\Platforms\Magento\Core\Api\App $app,
        \BeeketingConnect\Platforms\Magento\Data\CustomerManager $customerManager
    ) {
        $this->app = $app;
        $this->customerManager = $customerManager;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->app->init();
        $customer = $observer->getCustomer();
        $this->app->sendRequestWebhook(
            \BeeketingConnect\Common\Webhook::CUSTOMER_DELETE,
            ['id' => $customer->getId()]
        );
    }
}
