<?php
/**
 * @author Beeketing <cuongbui@beeketing.com>
 */

namespace BeeketingConnect\Platforms\Magento\Observer\Store;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use \BeeketingConnect\Platforms\Magento\Core\Api\App;
use \BeeketingConnect\Platforms\Magento\Data\ShopManager;
use \BeeketingConnect\Common\Webhook;

class StoreUpdateInformation implements ObserverInterface
{
    /**
     * @var App
     */
    private $app;
    private $shopManager;

    /**
     * OrderSaveAfter constructor.
     * @param App $app
     * @param ShopManager $shopManager
     */
    public function __construct(App $app, ShopManager $shopManager)
    {
        $this->app = $app;
        $this->shopManager = $shopManager;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $this->app->init();
        $data = $this->shopManager->get();
        $this->app->sendRequestWebhook(Webhook::UPDATE_SETTING_GENERAL, $data);
    }
}
