<?php
/**
 * Observer sales_order_save_after
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace BeeketingConnect\Platforms\Magento\Observer\Sales;

class OrderSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \BeeketingConnect\Platforms\Magento\Core\Api\App
     */
    private $app;
    /**
     * @var \BeeketingConnect\Platforms\Magento\Data\OrderManager
     */
    private $orderManager;

    /**
     * OrderSaveAfter constructor.
     * @param \BeeketingConnect\Platforms\Magento\Core\Api\App $app
     * @param \BeeketingConnect\Platforms\Magento\Data\OrderManager $orderManager
     */
    public function __construct(
        \BeeketingConnect\Platforms\Magento\Core\Api\App $app,
        \BeeketingConnect\Platforms\Magento\Data\OrderManager $orderManager
    ) {
    
        $this->app = $app;
        $this->orderManager = $orderManager;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->app->init();
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getOrder();

        // Set store scope
        $storeId = $order->getStoreId();
        $this->app->getSettingHelper()->setStoreId($storeId);

        $content = $this->orderManager->formatOrder($order);
        $this->app->sendRequestWebhook(\BeeketingConnect\Common\Webhook::ORDER_UPDATE, $content);
    }
}
