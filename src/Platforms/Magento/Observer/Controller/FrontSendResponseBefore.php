<?php
/**
 * FrontSendResponseBefore observer
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace BeeketingConnect\Platforms\Magento\Observer\Controller;

use BeeketingConnect\Common\Data\Exception\EntityNotFoundException;
use BeeketingConnect\Common\Data\Model\Cart;
use BeeketingConnect\Common\Data\Model\Collect;
use BeeketingConnect\Common\Data\Model\Collection;
use BeeketingConnect\Common\Data\Model\Customer;
use BeeketingConnect\Common\Data\Model\Order;
use BeeketingConnect\Common\Data\Model\Product;
use BeeketingConnect\Common\Data\Model\Variant;
use BeeketingConnect\Common\Constants as CommonConstants;
use BeeketingConnect\Platforms\Magento\Constants;
use BeeketingConnect\Platforms\Magento\Helper\Setting;
use BeeketingConnect\Platforms\Magento\Data\ResourceManager;
use Symfony\Component\HttpFoundation\Request;
use BeeketingConnect\Platforms\Magento\Helper\Response;

class FrontSendResponseBefore implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * Bridge api
     *
     * @var ResourceManager
     */
    private $resourceManager;

    /**
     * Setting helper
     *
     * @var \BeeketingConnect\Platforms\Magento\Helper\Setting
     */
    private $settingHelper;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    private $productMetadata;

    /**
     * @var \Magento\Framework\App\Response\Http\Interceptor
     */
    private $response;

    /**
     * @var array
     */
    private $specialResources = [
        'test_resource' => [Request::METHOD_GET, Request::METHOD_POST],
        'setting'       => [Request::METHOD_GET, Request::METHOD_PUT],
        'install_app'   => [Request::METHOD_POST],
    ];

    private $getManyResources = [
        'products',
        'customers',
        'orders',
        'variants',
        'products_variants',
        'collections',
        'collects',
    ];

    private $countResources = [
        'products_count',
        'customers_count',
        'orders_count',
        'collections_count',
        'collects_count',
    ];
    /**
     * FrontSendResponseBefore constructor.
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     * @param ResourceManager $resourceManager
     * @param Setting $settingHelper
     */
    public function __construct(
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        ResourceManager $resourceManager,
        Setting $settingHelper,
        Response $response
    ) {
        $this->productMetadata = $productMetadata;
        $this->resourceManager = $resourceManager;
        $this->settingHelper = $settingHelper;
        $this->response = $response;
        $this->response->setHeader('Content-Type', 'application/json; charset=utf-8');
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // Handle resource when $_GET['resource'] existed
        if ($this->getRequest()->query->get('resource')) {
            $this->settingHelper->setAppSettingKey(Constants::BEEKETING_CONNECT_KEY);
            $action = $observer->getEvent()->getControllerAction();
            $action->getActionFlag()->set(
                '',
                'no-dispatch',
                true
            );
            $this->resourceHandler($this->getRequest()->query->get('resource'));
        }
    }

    /**
     * @param \Magento\Framework\App\Response\Http $response
     */
    private function setResponseHandler($response)
    {
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');
        $this->response = $response;
    }

    /**
     * @return Request
     */
    private function getRequest()
    {
        if (!$this->request) {
            $this->request = Request::createFromGlobals();
        }

        return $this->request;
    }

    /**
     * Handle ?resource=* for file sync, cart process, inventory change, etc
     * @param $resource
     * @param \Magento\Framework\App\Response\Http $response
     */
    private function resourceHandler($resource)
    {
        // Print debug
        if ($resource === 'beeketing_debug') {
            $this->printDebug();
            return;
        }

        // Authenticate and routing to correct handler
        $request = $this->getRequest();
        $this->verifyAuth($request);

        // Special request for settings (get_setting, put_setting) and post_install_app
        if (in_array($resource, array_keys($this->specialResources))) {
            $this->handleInPluginRequests($resource, $request);
            return;
        }

        // Handle request using resource manager (common for all platforms)
        list($modelName, $method) = $this->getModelAndMethod($resource, $request->getMethod(), $request);

        // If not dev env, show error in json nicely
        if (BEEKETINGMAGENTO_ENVIRONMENT !== 'dev') {
            try {
                $result = $this->resourceManager->handleRequest($modelName, $method, $request, true);
                $this->response($result);
            } catch (EntityNotFoundException $e) {
                $this->responseError($e->getMessage(), 404);
            } catch (\Exception $e) {
                $this->responseError($e->getMessage());
            }
        }
    }

    /**
     * @param $request
     */
    private function verifyAuth($request)
    {
        // If this is authenticated via access token
        if ($requestApiKey = $request->headers->get(CommonConstants::VALIDATE_HEADER_API_KEY)) {
            // ~> allow for all
            $apiKey = $this->settingHelper->getSettings(CommonConstants::SETTING_API_KEY);
            if ($apiKey !== $requestApiKey) {
                $this->responseUnauthorized();
                return;
            }

            $this->resourceManager->allowedAccess = [Cart::$MODEL];
        } else {
            // Get request access token
            if (!$requestAccessToken = $request->headers->get(CommonConstants::VALIDATE_HEADER_ACCESS_TOKEN)) {
                $requestAccessToken = $request->query->get('access_token');
            }

            if ($requestAccessToken) {
                // Else if this is authenticated via API Key
                $accessToken = $this->settingHelper->getSettings(CommonConstants::SETTING_ACCESS_TOKEN);

                if ($accessToken !== $requestAccessToken) {
                    $this->responseUnauthorized();
                    return;
                }

                // ~> only allow cart model
                $this->resourceManager->allowedAccess = ['all'];
            } else {
                // Else no valid key provided
                // show error
                $this->responseUnauthorized('API key or access token is not provided');
            }
        }
    }

    /**
     * Get model name and method from current request to call into resource manager
     * This code is unusual but it is fallback for old route system of woocommerce plugin
     *
     * @param $resource
     * @param $requestMethod
     * @param Request $request
     * @return array
     */
    private function getModelAndMethod($resource, $requestMethod, Request $request)
    {
        // By default return default value
        $modelName = $resource;
        $method = $request->query->get('method') ? $request->query->get('method') : $requestMethod;
        $isGetMethod = $requestMethod === Request::METHOD_GET;

        if ($isGetMethod) {
            // Handle get many resource request
            if (in_array($resource, $this->getManyResources)) {
                $modelName = $this->getResourceModel($resource);
                if (!$request->get('resource_id')) {
                    $method = 'getMany';
                }
            }

            // Handle count resource request
            if (in_array($resource, $this->countResources)) {
                $modelName = $this->getResourceModel($resource);
                $method = 'count';
            }
        }

        return [
            $modelName, $method
        ];
    }

    /**
     * @param $resource
     * @return string
     */
    private function getResourceModel($resource)
    {
        $resourceModelMap = [
            'products'          => Product::$MODEL,
            'customers'         => Customer::$MODEL,
            'orders'            => Order::$MODEL,
            'variants'          => Variant::$MODEL,
            'products_variants' => Variant::$MODEL,
            'collections'       => Collection::$MODEL,
            'collects'          => Collect::$MODEL,

            // Count resource
            'products_count'    => Product::$MODEL,
            'customers_count'   => Customer::$MODEL,
            'orders_count'      => Order::$MODEL,
            'collections_count' => Collection::$MODEL,
            'collects_count'    => Collect::$MODEL,
        ];

        return isset($resourceModelMap[$resource]) ? $resourceModelMap[$resource] : '';
    }

    /**
     * Response unauthorized
     * @param null $message
     */
    private function responseUnauthorized($message = null)
    {
        if (!$message) {
            $message = 'Unauthorized wrong api key or access token';
        }

        $this->responseError($message, 401);
    }

    /**
     * Response json
     * @param array $jsonData
     */
    private function response($jsonData)
    {
        $this->response->clearBody();
        $this->response->setBody(json_encode($jsonData));
        $this->response->sendResponse();
    }

    /**
     * Response error
     * @param null $message
     * @param int $status
     */
    private function responseError($message, $status = 500)
    {
        $this->response->clearBody();
        $this->response->setStatusCode($status);
        $this->response->setBody(json_encode([
            'error' => $message,
        ]));
        $this->response->sendResponse();
    }

    /**
     * Handle special request only used inside this plugin
     * @param $resource
     * @param $request
     */
    private function handleInPluginRequests($resource, $request)
    {
        $requestMethod = strtoupper($request->getMethod());
        $allowedMethods = $this->specialResources[$resource];

        if (!in_array($requestMethod, $allowedMethods)) {
            $this->responseError('Method not allowed', 405);
            return;
        }

        if ($resource === 'setting') {
            $this->handleSettingRequest($requestMethod, $request);
            return;
        }

        if ($resource === 'install_app') {
            // Do nothing as new flow skip this logic
            $this->response([
                'setting' => [
                    'access_token' => $this->settingHelper->getAccessToken(),
                ],
            ]);
        }

        if ($resource === 'test_resource') {
            $this->response(array(
                'success' => true,
                'message' => 'message received',
            ));
        }
    }

    /**
     * @param $requestMethod
     * @param $request
     */
    private function handleSettingRequest($requestMethod, $request)
    {
        // get_setting
        if ($requestMethod === Request::METHOD_GET) {
            $this->response([
                'setting' => $this->settingHelper->getSettings(),
            ]);
            return;
        }

        if ($requestMethod === Request::METHOD_PUT) {
            // Validate
            if (!$request->getContent()) {
                $this->responseError('Setting data is not valid');
                return;
            }

            $content = json_decode($request->getContent(), true);
            if (!isset($content['setting']) && !$request->getContent()) {
                $this->responseError('Setting data is not valid - 2');
                return;
            }

            // Update setting
            foreach ($content['setting'] as $setting => $value) {
                $this->settingHelper->updateSettings($setting, $value);
            }

            $this->response([
                'setting' => $this->settingHelper->getSettings(),
            ]);
        }
    }

    /**
     * Print debug
     */
    private function printDebug()
    {
        //Updated to use object manager
        $this->response([
            'magento_version' => $this->productMetadata->getVersion(),
            'plugin_version' => BEEKETINGMAGENTO_VERSION,
            'php_version' => phpversion(),
            'beeketing_plugin_connected' => $this->settingHelper->getSettings(CommonConstants::SETTING_API_KEY)
                ? true
                : false,
            'beeketing_shop_id' => $this->settingHelper->getSettings(CommonConstants::SETTING_SHOP_ID),
        ]);
    }
}
