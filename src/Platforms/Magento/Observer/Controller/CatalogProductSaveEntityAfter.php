<?php
/**
 * Observer controller_action_catalog_product_save_entity_after
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace BeeketingConnect\Platforms\Magento\Observer\Controller;

use BeeketingConnect\Common\Webhook;
use BeeketingConnect\Platforms\Magento\Core\Api\App;
use BeeketingConnect\Platforms\Magento\Data\ProductManager;

class CatalogProductSaveEntityAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \BeeketingConnect\Platforms\Magento\Core\Api\App
     */
    private $app;

    /**
     * @var \BeeketingConnect\Platforms\Magento\Data\ProductManager
     */
    private $productManager;

    /**
     * FrontSendResponseBefore constructor.
     *
     * @param \BeeketingConnect\Platforms\Magento\Core\Api\App $app
     * @param \BeeketingConnect\Platforms\Magento\Data\ProductManager $productManager
     */
    public function __construct(
        App $app,
        ProductManager $productManager
    ) {
        $this->app = $app;
        $this->productManager = $productManager;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->app->init();
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getProduct();
        if ($product && $product->isVisibleInSiteVisibility()) {
            $content = $this->productManager->formatProduct($product);
            $storeIds = $product->getStoreIds();
            foreach ($storeIds as $storeId) {
                // Set store scope
                $this->app->getSettingHelper()->setStoreId($storeId);
                $this->app->sendRequestWebhook(Webhook::PRODUCT_UPDATE, $content);
            }
        }
    }
}
