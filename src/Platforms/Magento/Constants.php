<?php

namespace BeeketingConnect\Platforms\Magento;

class Constants
{
    // Key to store beeketing setting in admin
    const APP_SETTING_KEY = 'beeketing_magento_settings';

    // SETTING Keys
    const SETTING_STORE_ID = 'store_id';

    const BEEKETING_CONNECT_KEY = 'beeketing/connect/settings';
    const BETTERCOUPONBOX_KEY = 'beeketing/bettercouponbox/settings';
    const SALESPOP_KEY = 'beeketing/salespop/settings';
    const QUICKFACEBOOKCHAT_KEY = 'beeketing/quickfacebookchat/settings';
    const HAPPYEMAIL_KEY = 'beeketing/happyemail/settings';
    const PERSONALIZEDRECOMMENDATION_KEY = 'beeketing/personalizedrecommendation/settings';
    const CHECKOUTBOOST_KEY = 'beeketing/checkoutboost/settings';
    const BOOSTSALES_KEY = 'beeketing/boostsales/settings';
    const MAILBOT_KEY = 'beeketing/mailbot/settings';
    const COUNTDOWNCART_KEY = 'beeketing/countdowncart/settings';
    const MOBILEWEBBOOST_KEY = 'beeketing/mobilewebboost/settings';
    const PUSHER_KEY = 'beeketing/pusher/settings';

    // Events
    const PLUGIN_EVENT_INSTALL = 'Magento - Install Ext';
    const PLUGIN_EVENT_UNINSTALL = 'Magento - Delete Ext';

    // Platform code
    const PLATFORM_CODE = 'magento';

    // Image size
    const DEFAULT_IMAGE_WIDTH = 240;
    const DEFAULT_IMAGE_HEIGHT = 300;
}
