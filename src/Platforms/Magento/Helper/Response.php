<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace BeeketingConnect\Platforms\Magento\Helper;

use Magento\Framework\App\Response\Http;
use \Magento\Framework\App\PageCache\NotCacheableInterface;

class Response extends Http implements NotCacheableInterface
{
}
