<?php

namespace BeeketingConnect\Platforms\Magento\Helper;

use BeeketingConnect\Common\Constants as CommonConstants;
use BeeketingConnect\Platforms\Magento\Constants;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Setting
{
    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    private $resourceConfig;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var string
     */
    private $appSettingKey;

    /**
     * @var string
     */
    private $storeId = null;

    /**
     * @var array
     */
    private static $settings = [];

    /**
     * @var Common
     */
    private $commonHelper;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * Setting constructor.
     * @param \Magento\Config\Model\ResourceModel\Config $config
     * @param ScopeConfigInterface $scopeConfig
     * @param CacheInterface $cache
     * @param StoreManagerInterface $storeManager
     * @param UrlInterface $url
     * @param Common $common
     */
    public function __construct(
        \Magento\Config\Model\ResourceModel\Config $config,
        ScopeConfigInterface $scopeConfig,
        CacheInterface $cache,
        StoreManagerInterface $storeManager,
        UrlInterface $url,
        Common $common
    ) {
        $this->resourceConfig = $config;
        $this->scopeConfig = $scopeConfig;
        $this->cache = $cache;
        $this->storeManager = $storeManager;
        $this->commonHelper = $common;
        $this->url = $url;
    }

    /**
     * Set app setting key
     *
     * @return string
     */
    public function getAppSettingKey()
    {
        return $this->appSettingKey;
    }

    /**
     * Set app setting key
     *
     * @param $appSettingKey
     */
    public function setAppSettingKey($appSettingKey)
    {
        $this->appSettingKey = $appSettingKey;
    }

    /**
     * Get store id
     *
     * @return string
     */
    public function getStoreId()
    {
        return $this->storeId;
    }

    /**
     * Set store id
     *
     * @param $storeId
     */
    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;
    }

    /**
     * App setting keys
     *
     * @return array
     */
    public static function settingKeys()
    {
        return [
            CommonConstants::BETTERCOUPONBOX => Constants::BETTERCOUPONBOX_KEY,
            CommonConstants::BOOSTSALES => Constants::BOOSTSALES_KEY,
            CommonConstants::CHECKOUTBOOST => Constants::CHECKOUTBOOST_KEY,
            CommonConstants::HAPPYEMAIL => Constants::HAPPYEMAIL_KEY,
            CommonConstants::MAILBOT => Constants::MAILBOT_KEY,
            CommonConstants::PERSONALIZEDRECOMMENDATION => Constants::PERSONALIZEDRECOMMENDATION_KEY,
            CommonConstants::QUICKFACEBOOKCHAT => Constants::QUICKFACEBOOKCHAT_KEY,
            CommonConstants::SALESPOP => Constants::SALESPOP_KEY,
            CommonConstants::MOBILEWEBBOOST => Constants::MOBILEWEBBOOST_KEY,
            CommonConstants::COUNTDOWNCART => Constants::COUNTDOWNCART_KEY,
            CommonConstants::PUSHER => Constants::PUSHER_KEY,
        ];
    }

    /**
     * Switch settings
     *
     * @param $appCode
     */
    public function switchSettings($appCode)
    {
        $settingKeys = self::settingKeys();
        if (isset($settingKeys[$appCode])) {
            $settingKey = $settingKeys[$appCode];
            $this->appSettingKey = $settingKey;
        }
    }

    /**
     * Get settings
     *
     * @param null $key
     * @param null $default
     * @return mixed
     */
    public function getSettings($key = null, $default = null)
    {
        $settings = isset(self::$settings[$this->appSettingKey]) ? self::$settings[$this->appSettingKey] : [];
        if (!$settings) {
            $storeId = $this->getStoreId();
            $settings = $this->scopeConfig->getValue(
                $this->appSettingKey,
                ScopeInterface::SCOPE_STORE,
                $storeId
            );
            // @codingStandardsIgnoreStart
            $settings = $settings ? unserialize($settings) : [];
            // @codingStandardsIgnoreEnd
        }

        // Get setting by key
        if ($key) {
            if (isset($settings[$key])) {
                return $settings[$key];
            }
            return $default;
        }

        return $settings;
    }

    /**
     * @param $key
     * @param $value
     * @return array|mixed
     * @throws NoSuchEntityException
     */
    public function updateSettings($key, $value)
    {
        $settings = isset(self::$settings[$this->appSettingKey]) ? self::$settings[$this->appSettingKey] : [];
        if (!$settings) {
            $settings = $this->getSettings();
        }
        $storeId = $this->getCurrentStoreId();
        $settings[$key] = $value;
        self::$settings[$this->appSettingKey] = $settings;
        $this->resourceConfig
            ->saveConfig($this->appSettingKey, serialize($settings), ScopeInterface::SCOPE_STORES, $storeId);

        // Clean cache
        $this->cache->clean([Config::CACHE_TAG]);

        return $settings;
    }

    /**
     * Delete current store settings
     */
    public function deleteSettings()
    {
        $storeId = $this->getCurrentStoreId();
        $this->resourceConfig->deleteConfig($this->appSettingKey, ScopeInterface::SCOPE_STORES, $storeId);
    }

    /**
     * Delete all store settings
     */
    public function deleteAllSettings()
    {
        $stores = $this->storeManager->getStores(true, false);
        foreach ($stores as $store) {
            $this->resourceConfig->deleteConfig(
                $this->appSettingKey,
                ScopeInterface::SCOPE_STORES,
                $store->getId()
            );
        }
    }

    /**
     * @return int|mixed
     * @throws NoSuchEntityException
     */
    public function getCurrentStoreId()
    {
        $settingStoreId = $this->getSettings(Constants::SETTING_STORE_ID);
        $storeId = $settingStoreId ? $settingStoreId : $this->storeManager->getStore()->getId();
        return $storeId;
    }

    /**
     * @param bool $generateNewKey
     * @return mixed|string
     * @throws NoSuchEntityException
     */
    public function getAccessToken($generateNewKey = false)
    {
        // Generate access token
        $token = $this->getSettings(CommonConstants::SETTING_ACCESS_TOKEN);
        if (!$token || $generateNewKey) {
            $token = Common::generateAccessToken();
            $this->updateSettings(CommonConstants::SETTING_ACCESS_TOKEN, $token);
        }
        return $token;
    }
}
