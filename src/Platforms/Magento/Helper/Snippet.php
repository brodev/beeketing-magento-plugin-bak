<?php

namespace BeeketingConnect\Platforms\Magento\Helper;

use BeeketingConnect\Common\Constants as CommonConstants;
use BeeketingConnect\Platforms\Magento\Constants;
use BeeketingConnect\Platforms\Magento\Core\Api\App;

class Snippet
{
    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    private $productMetadata;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    private $httpRequest;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $session;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $urlHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var \Magento\Backend\Model\Auth
     */
    private $auth;

    /**
     * @var Common
     */
    private $commonHelper;

    /**
     * @var Setting
     */
    private $setting;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var App
     */
    private $app;

    /**
     * Snippet constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Framework\App\Request\Http $httpRequest
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\UrlInterface $urlHelper
     * @param \Magento\Backend\Model\Auth $auth
     * @param \Magento\Customer\Model\SessionFactory $session
     * @param Common $common
     * @param Setting $setting
     * @param App $app
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\App\Request\Http $httpRequest,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\UrlInterface $urlHelper,
        \Magento\Backend\Model\Auth $auth,
        \Magento\Customer\Model\SessionFactory $session,
        Common $common,
        Setting $setting,
        App $app
    ) {
        $this->storeManager = $storeManager;
        $this->priceCurrency = $priceCurrency;
        $this->httpRequest = $httpRequest;
        $this->productMetadata = $productMetadata;
        $this->registry = $registry;
        $this->urlHelper = $urlHelper;
        $this->session = $session;
        $this->auth = $auth;
        $this->commonHelper = $common;
        $this->setting = $setting;
        $this->app = $app;
    }

    /**
     * Init Setting
     */
    public function init()
    {
        $this->setting->setAppSettingKey(Constants::BEEKETING_CONNECT_KEY);
    }

    /**
     * Get Shop Api from setting
     * @return string
     */
    public function getApiKey()
    {
        if (!$this->apiKey) {
            $this->apiKey = $this->setting->getSettings(
                CommonConstants::SETTING_API_KEY
            );
        }

        return $this->apiKey;
    }

    /**
     * @return Setting
     */
    private function getSettingHelper()
    {
        return $this->setting;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPageSnippetData()
    {
        $settingHelper = $this->getSettingHelper();
        $request = $this->httpRequest;
        $customerSession = $this->session->create();

        $priceCurrency = $this->priceCurrency;
        $formattedPrice = $priceCurrency->format('11.11', true);
        $formattedPrice = preg_replace(
            '/[1]+[.,]{0,1}[1]+/',
            '{{amount}}',
            $formattedPrice,
            1
        );
        $currencyCode = $priceCurrency->getCurrency()->getCurrencyCode();
        $currencySymbol = $priceCurrency->getCurrency()->getCurrencySymbol();

        $store = $this->storeManager->getStore();
        $currencyRate = $store->getCurrentCurrencyRate();
        $baseCurrency = $store->getBaseCurrency()->getCode();

        $data = [];
        $data['magento_version'] = $this->productMetadata->getVersion();
        $data['app_setting_key'] = $settingHelper->getAppSettingKey();
        $data['plugin_version'] = BEEKETINGMAGENTO_VERSION;
        $data['php_version'] = phpversion();

        // Currency
        $data['currency'] = [
            'base_code' => $baseCurrency,
            'code' => $currencyCode,
            'symbol' => $currencySymbol,
            'format' => htmlentities($formattedPrice),
            'rate' => $currencyRate,
        ];

        // Page url
        $data['page_url'] = [
            'home' => $this->urlHelper->getBaseUrl(),
            'cart' => $this->urlHelper->getUrl('checkout/cart'),
            'checkout' => $this->urlHelper->getUrl('checkout'),
        ];

        // Customer
        if ($customerSession->isLoggedIn()) {
            $data['customer'] = [
                'id' => $customerSession->getCustomer()->getId(),
            ];
        }

        // Page
        $data['page'] = [];
        switch ($request->getFullActionName()) {
            case 'cms_index_index':
                $data['page']['type'] = 'home';
                break;
            case 'checkout_cart_index':
                $data['page']['type'] = 'cart';
                break;
            case 'checkout_index_index':
                $data['page']['type'] = 'checkout';
                break;
            case 'catalog_product_view':
                $currentProduct = $this->registry->registry('current_product');
                $data['page']['type'] = 'product';
                $data['page']['id'] = (int)$currentProduct->getId();
                break;
            case 'catalog_category_view':
                $currentCategory = $this->registry->registry('current_category');
                $data['page']['type'] = 'collection';
                $data['page']['id'] = (int)$currentCategory->getId();
                break;
            case 'checkout_onepage_success':
            case 'multishipping_checkout_success':
                $data['page']['type'] = 'post_checkout';
                break;
        }

        // Convert to js snippet
        $data = json_encode($data);
        $snippet = '<script>var _beeketing = JSON.parse(\'' . $data . '\');</script>';

        return $snippet;
    }

    /**
     * Get beeketing script
     * @param string $apiKey
     * @return string
     */
    public function getBeeketingScript($apiKey = '')
    {
        if ($apiKey == '') {
            $apiKey = $this->getApiKey();
        }

        return "
            <script>
              var bkRawUrl = function(){return window.location.href}();
              (function (win, doc, scriptPath, apiKey){
                function go(){
                  if (doc.getElementById(apiKey)) {return;}
                  var sc, node, today=new Date(),dd=today.getDate(),mm=today.getMonth()+1,yyyy=today.getFullYear();
                  if(dd<10)dd='0'+dd;if(mm<10)mm='0'+mm;today=yyyy+mm+dd;
                  window.BKShopApiKey =  apiKey;
                  sc = doc.createElement('script');
                  sc.src = scriptPath + '?' + today;
                  sc.id = apiKey;
                  node = doc.getElementsByTagName('script')[0];
                  node.parentNode.insertBefore(sc, node);
                }
                if(win.addEventListener){win.addEventListener('load', go, false);}
                else if(win.attachEvent){win.attachEvent('onload', go);}
              })(window, document, '" . BEEKETINGMAGENTO_SDK_PATH . "', '" . $apiKey . "');
            </script>
        ";
    }

    /**
     * @param $path
     * @param bool $addDailyCache
     * @return string
     */
    public function getConnectDashboardUrl($path, $addDailyCache = true)
    {
        return BEEKETINGMAGENTO_CONNECT_DASHBOARD . '/' . $path . ($addDailyCache ? '?v' . date('Ymd') : '');
    }

    /**
     * @param $path
     * @param bool $addDailyCache
     * @return string
     */
    public function getAnalyticsUrl($addDailyCache = true)
    {
        return BEEKETINGMAGENTO_ANALYTICS . ($addDailyCache ? '?v' . date('Ymd') : '');
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getJsAppData()
    {
        $settingHelper = $this->getSettingHelper();
        $data = [
            'plugin_url' => 'Beeketing_MagentoConnect',
            'api_key' => $settingHelper->getSettings(CommonConstants::SETTING_API_KEY),
            'access_token' => $settingHelper->getAccessToken(),
            'auto_login_token' => $settingHelper->getSettings(CommonConstants::SETTING_AUTO_LOGIN_TOKEN),
            'shop_id' => $settingHelper->getSettings(CommonConstants::SETTING_SHOP_ID),
            'beeketing_user_id' => $settingHelper->getSettings(CommonConstants::SETTING_USER_ID),
            'user_display_name' => $this->auth->getUser()->getFirstName(),
            'user_email' => $this->auth->getUser()->getEmail(),
            'site_url' => $this->storeManager->getStore()->getBaseUrl(),
            'domain' => $this->commonHelper->getShopDomain(),
            'env' => BEEKETINGMAGENTO_ENVIRONMENT,
            'connect_admin_url' => $this->urlHelper->getUrl('beeketing'),
            'platform' => Constants::PLATFORM_CODE,
            'admin_url' => $this->urlHelper->getUrl('adminhtml/module/action'),
            'ajax_url' => $this->urlHelper->getUrl('beeketing/index/ajax'),
            'plugin' => Constants::PLATFORM_CODE,
            'plugin_name' => 'Beeketing Magento',
            'is_woocommerce_active' => true,
            'woocommerce_plugin_url' => '',
        ];

        return $data;
    }

}
