<?php

namespace BeeketingConnect\Platforms\Magento\Helper;

class Common
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * Common constructor.
     * @param \Magento\Framework\UrlInterface $url
     */
    public function __construct(
        \Magento\Framework\UrlInterface $url
    ) {
        $this->url = $url;
    }

    /**
     * Get shop domain
     *
     * @return string
     */
    public function getShopDomain()
    {
        $siteUrl = $this->url->getBaseUrl();
        $urlParsed = parse_url($siteUrl);
        $host = isset($urlParsed['host']) ? $urlParsed['host'] : '';

        // Config www
        $config = filter_input(INPUT_GET, 'www');
        if ($config != null) {
            if (in_array($config, [0, false])) {
                $host = preg_replace('/^www\./', '', $host);
            } elseif (!preg_match('/^www\./', $host) && in_array($config, [1, true])) {
                $host = 'www.' . $host;
            }
        }

        return $host;
    }

    /**
     * Get shop absolute path
     *
     * @return string
     */
    public function getShopAbsolutePath()
    {
        return $this->url->getBaseUrl();
    }

    /**
     * Is beeketing hidden name
     *
     * @param $name
     * @return bool
     */
    public static function isBeeketingHiddenName($name)
    {
        if ((bool)preg_match('/\(BK (\d+)\)/', $name, $matches)) {
            return true;
        }

        return false;
    }

    /**
     * Get media base url
     *
     * @return mixed
     */
    public static function getMediaBaseUrl()
    {
        // @codingStandardsIgnoreStart
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $om->get('Magento\Store\Model\StoreManagerInterface');
        $currentStore = $storeManager->getStore();
        // @codingStandardsIgnoreEnd

        return $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * Get product image url
     *
     * @param $imagePath
     * @return string
     */
    public static function getProductImageUrl($imagePath)
    {
        return self::getMediaBaseUrl() . 'catalog/product' . $imagePath;
    }

    /**
     * Format price
     *
     * @param $price
     * @return string
     */
    public static function formatPrice($price)
    {
        return number_format($price, 2, '.', '');
    }

    /**
     * Generate access token
     *
     * @return string
     */
    public static function generateAccessToken()
    {
        try {
            if (function_exists('random_bytes')) {
                $string = random_bytes(16);
            } else {
                $string = openssl_random_pseudo_bytes(16);
            }

            $token = bin2hex($string);
        } catch (\Exception $e) {
            $token = hash('md5', uniqid(rand(), true));
        }

        return $token;
    }
}
