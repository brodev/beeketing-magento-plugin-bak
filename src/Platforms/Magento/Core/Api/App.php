<?php
/**
 * App api
 *
 * @author Beeketing <hi@beeketing.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace BeeketingConnect\Platforms\Magento\Core\Api;

use BeeketingConnect\Common\BeeketingAPI;
use BeeketingConnect\Common\Webhook;
use BeeketingConnect\Common\Constants as CommonConstants;
use BeeketingConnect\Platforms\Magento\Constants;
use BeeketingConnect\Platforms\Magento\Helper\Common;
use BeeketingConnect\Platforms\Magento\Helper\Setting;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Store\Model\StoreManagerInterface;

class App extends BeeketingAPI
{

    private $appCode;

    /**
     * @var string
     */
    private $beeketingPath;

    /**
     * @var string
     */
    private $beeketingApi;

    /**
     * @var Common
     */
    private $helper;

    /**
     * Setting helper
     *
     * @var Setting
     */
    private $settingHelper;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var Webhook
     */
    private $webhook;

    /**
     * App constructor.
     * @param StoreManagerInterface $storeManager
     * @param PriceCurrencyInterface $priceCurrency
     * @param Setting $settingHelper
     * @param Common $commonHelper
     * @param Webhook $webhook
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        PriceCurrencyInterface $priceCurrency,
        Setting $settingHelper,
        Common $commonHelper,
        Webhook $webhook
    ) {
        parent::__construct(BEEKETINGMAGENTO_GO_API, BEEKETINGMAGENTO_API);

        $this->settingHelper = $settingHelper;
        $this->storeManager = $storeManager;
        $this->priceCurrency = $priceCurrency;
        $this->webhook = $webhook;

        // Helper
        $this->helper = $commonHelper;

        $this->beeketingPath = BEEKETINGMAGENTO_PATH;
        $this->beeketingApi = BEEKETINGMAGENTO_API;
    }

    /**
     * Init app
     */
    public function init()
    {
        // Set setting helper
        $this->settingHelper->setAppSettingKey(
            Constants::BEEKETING_CONNECT_KEY
        );

        // Set api key
        $apiKey = $this->settingHelper->getSettings(
            CommonConstants::SETTING_API_KEY
        );

        $this->webhook->setApiKey($apiKey);
        $this->setApiKey($apiKey);
    }

    /**
     * @param $userId
     * @param $autoLoginToken
     * @return bool|mixed
     * @throws NoSuchEntityException
     */
    public function registerShop($userId, $autoLoginToken)
    {
        $result = $this->updateShopInfo();
        $shopResult = $this->getShopByApikey();

        // Install app success
        if ($result && isset($shopResult['id'])) {
            $this->settingHelper->updateSettings(CommonConstants::SETTING_API_KEY, $this->getApiKey());
            $this->settingHelper->updateSettings(CommonConstants::SETTING_SHOP_ID, $shopResult['id']);
            $this->settingHelper->updateSettings(CommonConstants::SETTING_USER_ID, $userId);
            $this->settingHelper->updateSettings(CommonConstants::SETTING_AUTO_LOGIN_TOKEN, $autoLoginToken);
            $this->settingHelper->updateSettings(
                CommonConstants::SETTING_SITE_URL,
                $this->helper->getShopAbsolutePath()
            );
            return $this->getApiKey();
        } else {
            $this->settingHelper->deleteSettings();
        }

        return false;
    }

    /**
     * Update shop absolute path
     * @param array $params
     * @return bool|mixed
     * @throws NoSuchEntityException
     */
    public function updateShopInfo($params = [])
    {
        $baseCurrency = $this->storeManager->getStore()->getBaseCurrency()->getCode();
        $formattedPrice = $this->priceCurrency->format(
            '11.11',
            true,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            null,
            $baseCurrency
        );
        $formattedPrice = preg_replace(
            '/[1]+[.,]{0,1}[1]+/',
            '{{amount}}',
            $formattedPrice,
            1
        );

        $params['absolute_path'] = $this->helper->getShopAbsolutePath();
        $params['currency'] = $baseCurrency;
        $params['currency_format'] = $formattedPrice;

        return parent::updateShopInfo($params);
    }

    /**
     * Get setting helper
     *
     * @return Setting
     */
    public function getSettingHelper()
    {
        return $this->settingHelper;
    }

    /**
     * @return Webhook
     */
    public function getWebhook()
    {
        return $this->webhook;
    }

    /**
     * Send tracking event
     *
     * @param array $params
     * @param null $email
     * @return bool
     */
    public function sendTrackingEvent($params = [], $email = null)
    {
        if ($this->apiKey) { // Track logged in shop
            $url = $this->getUrl('shops/track');
            $result = $this->post($url, $params);
            if (isset($result['hit']) && $result['hit']) { // Install successfully
                return true;
            }
        } elseif (isset($params['event'])) { // Track shop not logged in
            $event = $params['event'];
            unset($params['event']);
            $this->sendEmailTrackingEvent([
                'event' => $event,
                'event_params' => array_merge($params, [
                    'platform' => Constants::PLATFORM_CODE,
                    'shop_domain' => $this->helper->getShopDomain(),
                ]),
            ], $email);
        }

        return false;
    }

    /**
     * Send email tracking event
     * @param array $params
     * @param null $email
     * @return bool
     */
    public function sendEmailTrackingEvent($params = [], $email = null)
    {
        $params['email'] = $email;
        $url = $this->getPlatformUrl('bk/analytic_tracking', $params);
        $result = $this->get($url);

        if (isset($result['success']) && $result['success']) {
            return true;
        }

        return false;
    }

    /**
     * Get platform url
     *
     * @param $path
     * @param array $params
     * @return string
     */
    private function getPlatformUrl($path, $params = [])
    {
        $url = $this->beeketingPath . '/' . $path;

        if ($params) {
            $url .= '?' . http_build_query($params, '', '&');
        }

        return $url;
    }

    /**
     * Send request webhook
     * @param $topic
     * @param array $content
     * @param array $headers
     * @return array|bool|mixed
     * @throws NoSuchEntityException
     */
    public function sendRequestWebhook($topic, $content = [], $headers = [])
    {
        $shopId = $this->settingHelper->getSettings(CommonConstants::SETTING_SHOP_ID);
        if (!$shopId) {
            // Get shop id by api key
            $apiKey = $this->settingHelper->getSettings(CommonConstants::SETTING_API_KEY);
            if ($apiKey) {
                $shopResult = $this->get($this->getUrl('shops/api/' . $apiKey));
                if ($shopResult && isset($shopResult['shop']['id'])) {
                    $shopId = $shopResult['shop']['id'];

                    // Update shop_id
                    if ($topic != Webhook::UNINSTALL) {
                        $this->settingHelper->updateSettings(CommonConstants::SETTING_SHOP_ID, $shopId);
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        return $this->webhook->send($topic, $shopId, Constants::PLATFORM_CODE, $content, $this->appCode, $headers);
    }

    /**
     * @param $url
     * @param array $params
     * @param array $headers
     * @param bool $useGoApi
     * @return mixed
     */
    public function get($url, $params = [], $headers = [], $useGoApi = true)
    {
        return $this->sendRequest(self::METHOD_GET, $url, $params, $headers, $useGoApi);
    }

    /**
     * Send post request
     *
     * @param $url
     * @param array $params
     * @param array $headers
     * @return array|bool
     */
    public function post($url, $params = [], $headers = [])
    {
        return $this->sendRequest(self::METHOD_POST, $url, $params, $headers);
    }

    /**
     * Send put request
     *
     * @param $url
     * @param array $params
     * @param array $headers
     * @return array|bool
     */
    public function put($url, $params = [], $headers = [])
    {
        return $this->sendRequest(self::METHOD_PUT, $url, $params, $headers);
    }

    /**
     * Send delete request
     *
     * @param $url
     * @param array $param
     * @param array $headers
     * @return array|bool
     */
    public function delete($url, $param = [], $headers = [])
    {
        return $this->sendRequest(self::METHOD_DELETE, $url, $param, $headers);
    }

    /**
     * Get request url
     *
     * @param $path
     * @param array $params
     * @param string $ext
     * @return string
     */
    public function getUrl($path, $params = [], $ext = '')
    {
        $url = $path . $ext;
        if ($params) {
            $url .= '?' . http_build_query($params, '', '&');
        }

        return $url;
    }

    /**
     * Detect domain change
     * @return bool
     * @throws NoSuchEntityException
     */
    public function detectDomainChange()
    {
        $settingSiteUrl = $this->settingHelper->getSettings(CommonConstants::SETTING_SITE_URL);
        $siteUrl = $this->helper->getShopAbsolutePath();
        if ($settingSiteUrl != $siteUrl) {
            if ($this->updateShopAbsolutePath()) {
                $this->settingHelper->updateSettings(CommonConstants::SETTING_SITE_URL, $siteUrl);
                return true;
            }
        }
        return false;
    }

    /**
     * Update shop absolute path
     * @return bool|mixed
     * @throws NoSuchEntityException
     */
    public function updateShopAbsolutePath()
    {
        return $this->updateShopInfo([
            'absolute_path' => $this->helper->getShopAbsolutePath(),
        ]);
    }
}
