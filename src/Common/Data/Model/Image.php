<?php
/**
 * User: Quan Truong
 * Email: quan@beeketing.com
 * Date: 8/15/18
 */

namespace BeeketingConnect\Common\Data\Model;

class Image
{

    /**
     * Model code
     */
    public static $MODEL = 'image';
    public static $MODELS = 'images';

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $src;
}
